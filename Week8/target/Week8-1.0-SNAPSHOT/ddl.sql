create table roles(id serial primary key, role_name varchar(1000));

create table users(id serial primary key,
username varchar(1000),
password varchar(1000),
role_id int,
foreign key (role_id) references roles(id));

create table files(id serial primary key,
file_name varchar(1000),
uuid UUID,
content bytea,
user_id int,
foreign key (user_id) references users(id));