package constants;

import lombok.Getter;

@Getter
public enum UserRoles {
    Guest(1),
    Admin(2);

    private int roleId;

    UserRoles(int roleId) {
        this.roleId = roleId;
    }
}
