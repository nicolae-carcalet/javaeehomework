package aop;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;

@Logged
@Interceptor
public class LoggedAspect {
    private static final Logger log = LoggerFactory.getLogger(LoggedAspect.class);

    @AroundInvoke
    public Object auditMethod(InvocationContext ctx) throws Exception {
        Object result = ctx.proceed();
        Method calledMethod = ctx.getMethod();
        log.debug("Called method {} and returned {}", calledMethod.getName(), result);
        return result;
    }
}
