package controller;

import domain.UserEntity;
import lombok.Data;
import repository.UserRepository;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Data
@Named("login")
@SessionScoped
public class LoginController implements Serializable {
    private static final String MAIN_PAGE_NAME = "main.xhtml";
    private static final String REGISTER_PAGE_NAME = "page.xhtml";

    private String enteredUsername;
    private String enteredPassword;
    private UserEntity loggedInUserEntity;

    @EJB
    private UserRepository userRepository;

    public String doLogin() {
        boolean userWithCredentialsFound = userRepository.userExists(enteredUsername, enteredPassword);
        displayLoginFeedbackToastr(userWithCredentialsFound);
        addLoggedInUserToFacesContextIfUserFound(userWithCredentialsFound);
        return userWithCredentialsFound ? MAIN_PAGE_NAME : REGISTER_PAGE_NAME;
    }

    private void addLoggedInUserToFacesContextIfUserFound(boolean userFound) {
        if (userFound) {
            loggedInUserEntity = userRepository.getUserEntityByCredentials(enteredUsername, enteredPassword);
        }
    }

    private void displayLoginFeedbackToastr(boolean userExists) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (userExists) {
            context.addMessage(null, new FacesMessage("Success", "Login successfully"));
        } else {
            context.addMessage(null, new FacesMessage("Fail", "Wrong credentials or the user doesn't exist"));
        }
    }
}
