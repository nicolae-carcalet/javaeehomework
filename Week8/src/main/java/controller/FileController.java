package controller;

import aop.Logged;
import constants.UserRoles;
import domain.FileEntity;
import domain.UserEntity;
import lombok.Data;
import org.primefaces.model.file.UploadedFile;
import repository.FileRepository;
import repository.UserRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.UUID;

@Data
@Named("files")
@RequestScoped
public class FileController {
    private UploadedFile uploadedFile;

    @EJB
    private FileRepository fileRepository;

    @EJB
    private UserRepository userRepository;

    @Inject
    private LoginController loginController;

    @Inject
    private UUID uuid;

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public List<FileEntity> findAllFiles() {
        return fileRepository.findAll();
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public boolean isGuest() {
        String guestRoleValue = UserRoles.Guest.name();
        String userRole = getUserRole();

        return userRole == null || userRole.equals(guestRoleValue);
    }

    @Logged
    public void uploadFile() {
        FileEntity fileEntity = buildFileEntity();
        fileRepository.save(fileEntity);
    }

    private FileEntity buildFileEntity() {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileName(uploadedFile.getFileName());
        fileEntity.setContent(uploadedFile.getContent());
        fileEntity.setUuid(uuid);
        UserEntity userEntity = userRepository.findById(getUserId());
        fileEntity.setUserEntity(userEntity);
        return fileEntity;
    }

    private Integer getUserId() {
        UserEntity userEntity = loginController.getLoggedInUserEntity();
        return userEntity.getId();
    }

    private String getUserRole() {
        UserEntity userEntity = loginController.getLoggedInUserEntity();
        return userEntity.getRoleId().getRoleName();
    }
}
