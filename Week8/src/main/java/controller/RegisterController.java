package controller;

import domain.RoleEntity;
import domain.UserEntity;
import lombok.Data;
import repository.UserRepository;
import repository.UserRoleRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;

@Data
@Named("register")
@RequestScoped
public class RegisterController {
    private String enteredUsername;
    private String enteredPassword;
    private String selectedRole;

    @EJB
    private UserRepository userRepository;

    @EJB
    private UserRoleRepository userRoleRepository;

    public void addUser() {
        UserEntity userEntity = buildUserEntity();
        userRepository.createUser(userEntity);
        displaySuccessFeedbackWithToastr();
    }

    private void displaySuccessFeedbackWithToastr() {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage("Success", "User added successfully");
        context.addMessage(null, message);
    }

    private UserEntity buildUserEntity() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(enteredUsername);
        userEntity.setPassword(enteredPassword);
        RoleEntity roleEntity = getUserRoleEntity();
        userEntity.setRoleId(roleEntity);
        return userEntity;
    }

    private RoleEntity getUserRoleEntity() {
        List<RoleEntity> roleEntities = userRoleRepository.findAll();
        for (RoleEntity roleEntity : roleEntities) {
            if(roleEntity.getRoleName().equals(selectedRole)) {
                return roleEntity;
            }
        }
        return null;
    }
}
