package config;

import javax.enterprise.inject.Produces;
import java.util.UUID;

public class FileUUID {
    @Produces
    public UUID getFileUUID() {
        return UUID.randomUUID();
    }
}
