package repository;

import domain.FileEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class FileRepository {
    private static final String GET_ALL_FILES_QUERY = "select f from FileEntity f";

    @PersistenceContext
    private EntityManager entityManager;

    public List<FileEntity> findAll() {
        return entityManager.createQuery(GET_ALL_FILES_QUERY, FileEntity.class).getResultList();
    }

    public void save(FileEntity fileEntity) {
        entityManager.persist(fileEntity);
    }
}
