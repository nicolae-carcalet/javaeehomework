package repository;

import domain.RoleEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class UserRoleRepository {
    private static final String GET_ALL_ROLES_QUERY = "select r from RoleEntity r";

    @PersistenceContext(unitName = "mypu")
    private EntityManager entityManager;

    public List<RoleEntity> findAll() {
        return entityManager.createQuery(GET_ALL_ROLES_QUERY, RoleEntity.class).getResultList();
    }
}
