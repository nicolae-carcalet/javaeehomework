package repository;

import domain.UserEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class UserRepository {

    private static final String USERNAME_PARAMETER = "username";
    private static final String PASSWORD_PARAMETER = "password";
    private static final String ID_PARAMETER = "id";
    private static final String USER_SELECT_QUERY = "select u from UserEntity as u" +
            " where u.username = :username and u.password = :password";
    private static final String USER_SELECT_BY_ID_QUERY = "select u from UserEntity as u where u.id = :id";

    @PersistenceContext
    private EntityManager entityManager;

    public UserEntity findById(int id) {
        TypedQuery<UserEntity> query = entityManager.createQuery(USER_SELECT_BY_ID_QUERY, UserEntity.class);
        query.setParameter(ID_PARAMETER, id);
        return query.getSingleResult();
    }

    public UserEntity getUserEntityByCredentials(String username, String password) {
        return findAllUsersWithCredentials(username, password).get(0);
    }

    public boolean userExists(String username, String password) {
        return !findAllUsersWithCredentials(username, password).isEmpty();
    }

    public List<UserEntity> findAllUsersWithCredentials(String username, String password) {
        TypedQuery<UserEntity> usernamePasswordSearchQuery = buildUserSearchQuery(username, password);
        return usernamePasswordSearchQuery.getResultList();
    }

    private TypedQuery<UserEntity> buildUserSearchQuery(String username, String password) {
        TypedQuery<UserEntity> query = entityManager.createQuery(USER_SELECT_QUERY, UserEntity.class);
        query.setParameter(USERNAME_PARAMETER, username);
        query.setParameter(PASSWORD_PARAMETER, password);
        return query;
    }

    public void createUser(UserEntity userEntity) {
        entityManager.persist(userEntity);
    }
}
