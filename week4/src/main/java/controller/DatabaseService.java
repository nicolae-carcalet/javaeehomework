package controller;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@ManagedBean(name = "databaseService", eager = true)
@ApplicationScoped
public class DatabaseService {
    private static final String DATASOUCE_URL_INIT_PARAM = "datasourceUrl";
    private static final String DATABASE_NAME_INIT_PARAM = "databaseName";
    private static final String USERNAME_INIT_PARAM = "db_username";
    private static final String PASSWORD_INIT_PARAM = "db_password";
    private static final String JDBC_DRIVER_CLASSNAME = "org.h2.Driver";

    private ExternalContext externalFacesContext;
    private Connection connection = null;

    public DatabaseService() throws SQLException, ClassNotFoundException {
        initFacesContext();
        createDatabaseConnection();
    }

    private void initFacesContext() {
        FacesContext context = FacesContext.getCurrentInstance();
        externalFacesContext = context.getExternalContext();
    }

    public Connection getDatabaseConnection() {
        return connection;
    }

    private void createDatabaseConnection() throws SQLException, ClassNotFoundException {
        loadJDBCDriver();
        String datasourceURL = externalFacesContext.getInitParameter(DATASOUCE_URL_INIT_PARAM);
        String databaseName = externalFacesContext.getInitParameter(DATABASE_NAME_INIT_PARAM);
        String username = externalFacesContext.getInitParameter(USERNAME_INIT_PARAM);
        String password = externalFacesContext.getInitParameter(PASSWORD_INIT_PARAM);
        String connectionString = getConnectionURL(datasourceURL, databaseName);
        connection = DriverManager.getConnection(connectionString, username, password);
    }

    private void loadJDBCDriver() throws ClassNotFoundException {
        Class.forName(JDBC_DRIVER_CLASSNAME);
    }

    private String getConnectionURL(String datasourceURL, String databaseName) {
        return datasourceURL + databaseName;
    }

    @Override
    public void finalize() throws SQLException {
        connection.close();
    }
}
