package controller;

import javax.faces.bean.*;

import model.MeetingEntity;
import model.PersonEntity;
import utils.MeetingUtils;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "meetings")
@SessionScoped
public class MeetingBackingBean {
    private static final String MEETINGS_RETRIEVAL_QUERY = "select * from meeting";
    private static final String PERSONS_SELECT_ALL_QUERY = "select * from persons";
    private static final String INSERT_MEETING_QUERY = "insert into meeting(topic, start_date, duration) values ('%s', '%s', '%s')";
    private static final String INSERTION_ID_QUERY = "select max(id) from meeting";
    private static final String LINKING_TABLE_INSERT_QUERY = "insert into linking(meeting_id, person_id) values (%d, %d)";

    private MeetingEntity newMeeting = new MeetingEntity(0, "", null, null);
    private List<String> selectedPersonsIds = new ArrayList<>();
    private List<MeetingEntity> allMeetings;

    @ManagedProperty(value = "#{databaseService}")
    private DatabaseService databaseService;

    public List<MeetingEntity> getAllMeetings() {
        try (Statement statement = databaseService.getDatabaseConnection().createStatement()) {
            List<MeetingEntity> meetingEntities = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(MEETINGS_RETRIEVAL_QUERY);
            while (resultSet.next()) {
                meetingEntities.add(MeetingUtils.getMeetingEntityFromResultSetCursor(resultSet));
            }
            return meetingEntities;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public void createMeeting() {
        try (Statement statement = databaseService.getDatabaseConnection().createStatement()) {
            String sqlQuery = getInsertMeetingQuery();
            statement.execute(sqlQuery);
            int meetingId = getNewMeetingInsertionId(statement);
            updateLinkingTable(statement, meetingId);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private String getInsertMeetingQuery() {
        String meetingTopic = newMeeting.getTopic();
        LocalDateTime meetingStartDate = newMeeting.getStartTime();
        LocalDateTime meetingDuration = newMeeting.getEndTime();
        return String.format(INSERT_MEETING_QUERY, meetingTopic, meetingStartDate, meetingDuration);
    }

    private int getNewMeetingInsertionId(Statement statement) throws SQLException {
        // HACK, FIX IT!
        ResultSet cursor = statement.executeQuery(INSERTION_ID_QUERY);
        if (cursor.next()) {
            return cursor.getInt(1);
        }
        return -1;
    }

    private void updateLinkingTable(Statement statement, int meetingId) throws SQLException {
        for (String selectedPersonId : selectedPersonsIds) {
            String linkingTableInsertQuery = String.format(LINKING_TABLE_INSERT_QUERY, meetingId, Integer.parseInt(selectedPersonId));
            statement.execute(linkingTableInsertQuery);
        }
    }

    public List<PersonEntity> getAllPersons() {
        List<PersonEntity> personEntities = new ArrayList<>();
        try (Statement statement = databaseService.getDatabaseConnection().createStatement()) {
            ResultSet cursor = statement.executeQuery(PERSONS_SELECT_ALL_QUERY);
            while (cursor.next()) {
                personEntities.add(getPersonEntityFromCurrentRow(cursor));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return personEntities;
    }

    private PersonEntity getPersonEntityFromCurrentRow(ResultSet cursor) throws SQLException {
        int personId = cursor.getInt(1);
        String personName = cursor.getString(2);
        return new PersonEntity(personId, personName);
    }

    public List<String> getSelectedPersonsIds() {
        return selectedPersonsIds;
    }

    public void setSelectedPersonsIds(List<String> selectedPersonsIds) {
        this.selectedPersonsIds = selectedPersonsIds;
    }

    public void setDatabaseService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public MeetingEntity getNewMeeting() {
        return newMeeting;
    }

    public void setNewMeeting(MeetingEntity meeting) {
        newMeeting = meeting;
    }
}
