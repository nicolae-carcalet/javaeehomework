package validators;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@FacesValidator("meetingTimeValidator")
public class MeetingTimeValidator implements Validator {

    private static final String START_TIME_FIELD_ATTRIBUTE_NAME = "startTime";
    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT);

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object endTimeField) throws ValidatorException {
        LocalDateTime startDateTime = getStartDateTime(uiComponent);
        LocalDateTime endDateTime = getEndDateTime(uiComponent);
        int x = 12;
    }

    private LocalDateTime getStartDateTime(UIComponent uiComponent) {
        UIInput startDateTimeInput = (UIInput) uiComponent.getAttributes().get(START_TIME_FIELD_ATTRIBUTE_NAME);
        return getDateTimeFromField(startDateTimeInput);
    }

    private LocalDateTime getEndDateTime(UIComponent endDateTimeComponent) {
        UIInput endDateTimeInput = (UIInput) endDateTimeComponent;
        return getDateTimeFromField(endDateTimeInput);
    }

    private LocalDateTime getDateTimeFromField(UIInput inputField) {
        String fieldValue = (String) inputField.getSubmittedValue();
        return LocalDateTime.parse(fieldValue);
    }
}
