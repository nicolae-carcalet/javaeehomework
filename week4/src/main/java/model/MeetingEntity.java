package model;


import java.time.LocalDateTime;

public class MeetingEntity {
    private int meetingId;
    private String topic;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public MeetingEntity(int meetingId, String topic, LocalDateTime startTime, LocalDateTime endTime) {
        this.meetingId = meetingId;
        this.topic = topic;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "MeetingEntity{" +
                "meetingId=" + meetingId +
                ", topic='" + topic + '\'' +
                ", start_time=" + startTime +
                ", duration=" + endTime +
                '}';
    }
}
