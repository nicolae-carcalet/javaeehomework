package solver;

import model.MeetingEntity;
import model.PersonEntity;
import org.chocosolver.solver.*;
import org.chocosolver.solver.variables.Variable;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class MeetingProblemSchedulingModelBuilder {
    private Model model;

    public Model buildModel(List<MeetingEntity> meetingEntities, List<PersonEntity> personEntities) {
        model = new Model();
        addVariables(meetingEntities);
        addConstraints(meetingEntities);
        return model;
    }

    private void addVariables(List<MeetingEntity> meetingEntities) {
        for (MeetingEntity meetingEntity : meetingEntities) {
            String unknownVariableName = "Meeting" + meetingEntity.getMeetingId();

            // TODO: This is VERY, VERY BAD. Change it
            int startTimeInMinutes = (int) meetingEntity.getStartTime().toEpochSecond(ZoneOffset.UTC) / 60;
            int endTimeInMinutes = (int) meetingEntity.getEndTime().toEpochSecond(ZoneOffset.UTC) / 60;
            model.intVar(unknownVariableName, startTimeInMinutes, endTimeInMinutes);
        }
    }

    private void addConstraints(List<MeetingEntity> meetingEntities) {

    }

    public static void main(String[] args) {
        MeetingProblemSchedulingModelBuilder meetingProblemSchedulingModelBuilder = new MeetingProblemSchedulingModelBuilder();
        List<MeetingEntity> entities = new ArrayList<>();
        entities.add(new MeetingEntity(1, "A", LocalDateTime.of(2020, 11, 4, 12, 0, 0),
                LocalDateTime.of(2020, 11, 4, 13, 0, 0)));
        entities.add(new MeetingEntity(2, "B", LocalDateTime.of(2020, 11, 4, 12, 0, 0),
                LocalDateTime.of(2020, 11, 4, 13, 0, 0)));
        entities.add(new MeetingEntity(3, "c", LocalDateTime.of(2020, 11, 4, 16, 0, 0),
                LocalDateTime.of(2020, 11, 4, 17, 0, 0)));
        entities.add(new MeetingEntity(4, "d", LocalDateTime.of(2020, 11, 4, 15, 0, 0),
                LocalDateTime.of(2020, 11, 4, 15, 30, 0)));

    }
}
