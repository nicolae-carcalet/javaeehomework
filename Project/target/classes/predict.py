import sys
import keras
import numpy as np
from os import path

SAVED_MODULE_FILE_NAME = "saved_model.pb"


def getAbsolutePathToSavedModel():
	absolutePathToScript = getAbsolutePathToScript()
	return path.join(absolutePathToScript, SAVED_MODULE_FILE_NAME)


def getAbsolutePathToScript():
	scriptPath = path.abspath(__file__)
	return path.dirname(scriptPath)


cmdLineArgs = [int(val) for val in sys.argv[1:]]
feedForwardData = np.array(cmdLineArgs).reshape(1, 5)
print(feedForwardData)
model = keras.models.load_model(getAbsolutePathToScript())
predictedValue = model.predict(feedForwardData)[0][0]
predictionFilePath = path.join(getAbsolutePathToScript(), "prediction.txt")
with open(predictionFilePath, "w") as outputFile:
	outputFile.write(str(predictedValue))
