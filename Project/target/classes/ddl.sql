create table house_prices(id serial primary key,
price int, surface_sq_meters int, location varchar(1000),
rooms int, construction_year int, floor_number int);

create table job_type(job_type_id serial primary key, type_name varchar(10));
create table job_status(job_status_id serial primary key, status_name varchar(10));

create table jobs(job_id serial primary key, job_type int, job_status int, started timestamp, finished timestamp, delay int,
constraint fk1 foreign key (job_type) references job_type(job_type_id),
constraint fk2 foreign key (job_status) references job_status(job_status_id));

insert into job_type(type_name) values ('Scrape'), ('Train');
insert into job_status(status_name) values ('Queued'), ('Running'), ('Finished'), ('Failed');