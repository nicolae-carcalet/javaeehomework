import keras
import pandas
from os import path
from sklearn.model_selection import train_test_split

TRAINING_DATASET_FILE_NAME = "data.csv"
TRAIN_COLUMNS = ['Price', 'Construction Year', 'Rooms Number', 'Floor Number', 'Location', 'Surface Area']
TARGET_COLUMN = "Price"


def getTrainData():
	data = initTraingDataframe()
	train_dataframe = data[TRAIN_COLUMNS]
	ranks = train_dataframe["Location"].rank(method="dense", ascending=False).astype(int)
	train_dataframe["Location"] = pandas.Series(ranks)
	return train_dataframe


def initTraingDataframe():
	trainingDataPath = getTrainingDataAbsolutePath()
	return pandas.read_csv(trainingDataPath)


def getTrainingDataAbsolutePath():
	absolutePathToScript = getAbsolutePathToScript()
	return path.join(absolutePathToScript, TRAINING_DATASET_FILE_NAME)


def getAbsolutePathToScript():
	scriptPath = path.abspath(__file__)
	return path.dirname(scriptPath)


def main():
	trainData = getTrainData()

	# min_max_scaler = MinMaxScaler()
	# trainData = pandas.DataFrame(min_max_scaler.fit_transform(trainData))

	model = keras.Sequential()
	model.add(keras.layers.Dense(64, input_shape=(1, 5), activation="relu"))
	model.add(keras.layers.Dense(128, activation="relu"))
	model.add(keras.layers.Dense(64, activation="relu"))
	model.add(keras.layers.Dense(1))

	model.compile(optimizer="rmsprop", loss="mse", metrics=[keras.metrics.MeanAbsoluteError()])

	# print(model.summary())

	# See this: https://predictivemodeler.com/2019/10/19/tensorflow-boston-house-prices/

	# Remove the target column(price)
	x_train = trainData.drop(TARGET_COLUMN, axis=1).values
	y_train = trainData[[TARGET_COLUMN]].values

	x_train, x_val_and_test, y_train, y_val_and_test = train_test_split(x_train, y_train, test_size=0.2)
	x_val, x_test, y_val, y_test = train_test_split(x_val_and_test, y_val_and_test, test_size=0.5)

	# print(x_train.shape)
	# print(y_train.shape)
	# print(x_val.shape)
	# print(y_val.shape)

	print(x_train.shape)
	# Save the first 10 instances for validation
	model.fit(x_train[10:], y_train[10:], validation_data=(x_val, y_val), batch_size=1, epochs=10, verbose=1)
	model.save(getAbsolutePathToScript())


# print(model.evaluate(x_test, y_test))
# print(x_test, y_test)
# print(model.predict(x_test))


if __name__ == "__main__":
	main()
