package scraper;

import domain.HouseOfferEntity;

import java.io.IOException;
import java.util.Set;

public interface HouseOfferSiteScraper {

    int getOfferPagesCount(String page) throws IOException;

    Set<HouseOfferEntity> getHouseOffers(String page) throws IOException;

    String getPageUrlForPageNumber(String baseUrl, int pageNumber);
}
