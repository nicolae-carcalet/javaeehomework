package scraper;

import domain.HouseOfferEntity;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.IntegerParsingUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ImobiliareROExtractor implements HouseOfferSiteScraper {

    private static final String NUMBER_OF_PAGES_CSS_SELECTOR = "div.index_paginare > a.butonpaginare";
    private static final String NUMBER_OF_PAGES_ATTRIBUTE = "data-pagina";
    private static final String HOUSE_OFFER_CSS_SELECTOR = "div.box-anunt";
    private static final String HOUSE_OFFER_DETAILS_CSS_SELECTOR = "ul.lista-tabelara";
    private static final String HOUSE_OFFER_PRICE_CSS_SELECTOR = "div.pret";
    private static final String HOUSE_OFFER_LOCATION_CSS_SELECTOR = "div.header_info > div.row";
    private static final String[] SUPPORTED_HOUSE_OFFER_DETAILS_FIELD = new String[]{"Etaj:", "Nr. camere:",
            "An construcţie:", "Suprafaţă utilă:"};

    public static void main(String[] args) throws IOException {
        ImobiliareROExtractor imobiliareROScraper = new ImobiliareROExtractor();
        String baseUrl = "https://www.imobiliare.ro/inchirieri-apartamente/bucuresti";
        int pages = imobiliareROScraper.getOfferPagesCount(baseUrl);
        System.out.println(pages);
        imobiliareROScraper.getHouseOffers(baseUrl);
    }

    @Override
    public int getOfferPagesCount(String baseUrl) throws IOException {
        Document document = Jsoup.connect(baseUrl).get();
        return scrapeOfferPagesCount(document);
    }

    public int scrapeOfferPagesCount(Document page) {
        Elements rawElement = page.select(NUMBER_OF_PAGES_CSS_SELECTOR);
        Element lastPageElement = rawElement.get(rawElement.size() - 2);
        String pagesCount = lastPageElement.attr(NUMBER_OF_PAGES_ATTRIBUTE);
        return Integer.parseInt(pagesCount);
    }

    public Set<HouseOfferEntity> getHouseOffers(String baseUrl) throws IOException {
        Document document = Jsoup.connect(baseUrl).get();
        return scrapeHouseOffers(document);
    }

    @Override
    public String getPageUrlForPageNumber(String baseUrl, int pageNumber) {
        return baseUrl + "?pagina=" + pageNumber;
    }

    public Set<HouseOfferEntity> scrapeHouseOffers(Document page) {
        Set<HouseOfferEntity> houseOffers = new HashSet<>();
        Elements houseOffersElements = page.select(HOUSE_OFFER_CSS_SELECTOR);
        houseOffersElements.forEach(element -> {
            try {
                Thread.sleep(1000);
                HouseOfferEntity houseOfferEntity = scrapeHouseOfferInformation(element);

                if (isHouseOfferValid(houseOfferEntity)) {
                    houseOffers.add(houseOfferEntity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return houseOffers;
    }

    private boolean isHouseOfferValid(HouseOfferEntity houseOfferEntity) {
        return houseOfferEntity.getPrice() != 0 && houseOfferEntity.getConstructionYear() != 0
                && houseOfferEntity.getFloorNumber() != 0 && houseOfferEntity.getLocation() != null
                && houseOfferEntity.getRoomsNumber() != 0 && houseOfferEntity.getSurfaceArea() != 0;
    }

    private String scrapeHouseOfferLocation(Document document) {
        Element headerContentElement = document.getElementById("content-detalii");
        Elements headerContentRootElement = headerContentElement.select(HOUSE_OFFER_LOCATION_CSS_SELECTOR);
        return buildLocationFromRawLocationHtml(headerContentRootElement);
    }

    private String buildLocationFromRawLocationHtml(Elements elements) {
        String[] locationTokens = getHouseOfferLocationTokens(elements);
        return concatLocationTokens(locationTokens);
    }

    private String concatLocationTokens(String[] locationTokens) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 2, len = locationTokens.length; i < len; i++) {
            stringBuilder.append(locationTokens[i]).append(" ");
        }

        return stringBuilder.toString().trim();
    }

    private String[] getHouseOfferLocationTokens(Elements elements) {
        String rawLocationText = elements.get(0).childNode(1).childNode(0).outerHtml();
        return rawLocationText.split(" ");
    }

    private int scrapeHouseOfferPrice(Document document) {
        Element element = document.selectFirst(HOUSE_OFFER_PRICE_CSS_SELECTOR);
        return IntegerParsingUtils.parseInt(element.childNode(1).outerHtml());
    }

    private HouseOfferEntity scrapeHouseOfferInformation(Element houseOfferElement) throws IOException {
        String fullLinkDetails = getFullDetailsLink(houseOfferElement);
        Document houseOfferDetailsPage = Jsoup.connect(fullLinkDetails).get();
        return buildHouseOffer(houseOfferDetailsPage);
    }

    private HouseOfferEntity buildHouseOffer(Document houseOfferDetailsPage) {
        Elements houseOfferDetails = houseOfferDetailsPage.select(HOUSE_OFFER_DETAILS_CSS_SELECTOR);
        HouseOfferEntity houseOffer = buildHouseOfferFromPage(houseOfferDetails);
        houseOffer.setLocation(scrapeHouseOfferLocation(houseOfferDetailsPage));
        houseOffer.setPrice(scrapeHouseOfferPrice(houseOfferDetailsPage));
        return houseOffer;
    }

    private HouseOfferEntity buildHouseOfferFromPage(Elements houseOfferDetails) {
        HouseOfferEntity houseOffer = new HouseOfferEntity();
        parseDetailsLists(houseOffer, houseOfferDetails);
        return houseOffer;
    }

    private void parseDetailsLists(HouseOfferEntity houseOffer, Elements houseOfferDetailsListsElements) {
        Element firstDetailsPage = houseOfferDetailsListsElements.get(0);
        getHouseOfferDetails(houseOffer, firstDetailsPage);
        Element secondDetailsPage = houseOfferDetailsListsElements.get(1);
        getHouseOfferDetails(houseOffer, secondDetailsPage);
    }

    private void getHouseOfferDetails(HouseOfferEntity houseOffer, Element detailsPageList) {
        for (Element childElement : detailsPageList.children()) {
            if (isLiElement(childElement)) {
                String key = getHouseOfferDetailsListKey(childElement);
                String value = getHouseOfferDetailsListValue(childElement);
                addKeyValueToHouseOfferIfSupported(key, value, houseOffer);
            }
        }
    }

    private void addKeyValueToHouseOfferIfSupported(String key, String value, HouseOfferEntity houseOffer) {
        if (!isSupportedKey(key)) {
            return;
        }

        int convertedValue = IntegerParsingUtils.parseInt(value);
        if (key.equals(SUPPORTED_HOUSE_OFFER_DETAILS_FIELD[0])) {
            houseOffer.setFloorNumber(convertedValue);
        } else if (key.equals(SUPPORTED_HOUSE_OFFER_DETAILS_FIELD[1])) {
            houseOffer.setRoomsNumber(convertedValue);
        } else if (key.equals(SUPPORTED_HOUSE_OFFER_DETAILS_FIELD[2])) {
            houseOffer.setConstructionYear(convertedValue);
        } else if (key.equals(SUPPORTED_HOUSE_OFFER_DETAILS_FIELD[3])) {
            houseOffer.setSurfaceArea(convertedValue);
        }
    }

    private boolean isSupportedKey(String key) {
        for (String supportedKey : SUPPORTED_HOUSE_OFFER_DETAILS_FIELD) {
            if (key.equals(supportedKey)) {
                return true;
            }
        }
        return false;
    }

    private String getHouseOfferDetailsListKey(Element detailsListElement) {
        return detailsListElement.childNode(0).outerHtml();
    }

    private String getHouseOfferDetailsListValue(Element detailsListElement) {
        return detailsListElement.childNode(1).childNode(0).outerHtml();
    }

    private boolean isLiElement(Element element) {
        return element.is("li");
    }

    private String getFullDetailsLink(Element houseOfferElement) {
        Elements houseOfferFullDetailsLinkElements = houseOfferElement.getElementsByClass("mobile-container-url");
        return houseOfferFullDetailsLinkElements.get(0).attr("href");
    }
}
