package scraper;

import domain.HouseOffer;
import domain.HouseOfferEntity;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.Set;

public interface HouseOfferSiteExtractor {

    int getNumberOfPages(Document page);

    List<String> getAllPages(Document page);

    Set<HouseOfferEntity> getHouseOffers(Document page);
}
