package repositories;

import domain.JobTypeEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class JobTypeRepository {
    private static final String GET_ALL_JOBS_QUERY = "select j from JobTypeEntity as j";

    @PersistenceContext(unitName = "DbPU")
    EntityManager entityManager;

    public List<JobTypeEntity> getAllEntities() {
        return entityManager.createQuery(GET_ALL_JOBS_QUERY, JobTypeEntity.class).getResultList();
    }
}
