package repositories;

import domain.HouseOfferEntity;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Stateless
public class HouseOfferRepository {
    private static final String GET_ALL_OFFERS_QUERY = "select houseOffer from HouseOfferEntity as houseOffer";
    private static final String OFFER_SEARCH_QUERY = "select houseOffer from HouseOfferEntity as houseOffer" +
            " where houseOffer.location = :location AND houseOffer.price = :price AND houseOffer.floorNumber = :floorNumber" +
            " AND houseOffer.roomsNumber = :roomsNumber AND houseOffer.surfaceArea = :surfaceArea" +
            " AND houseOffer.constructionYear = :constructionYear";

    @PersistenceContext(unitName = "DbPU")
    EntityManager entityManager;

    public void addAll(Collection<HouseOfferEntity> houseOffers) {
        for (HouseOfferEntity houseOfferEntity : houseOffers) {
            addHouseOffer(houseOfferEntity);
        }
    }

    public void addHouseOffer(HouseOfferEntity houseOfferEntity) {
        entityManager.persist(houseOfferEntity);
    }

    public boolean houseOfferExists(HouseOfferEntity houseOfferEntity) {
        try {
            TypedQuery<HouseOfferEntity> searchQuery = entityManager.createQuery(OFFER_SEARCH_QUERY, HouseOfferEntity.class);
            searchQuery = searchQuery.setParameter("location", houseOfferEntity.getLocation());
            searchQuery = searchQuery.setParameter("price", houseOfferEntity.getPrice());
            searchQuery = searchQuery.setParameter("floorNumber", houseOfferEntity.getFloorNumber());
            searchQuery = searchQuery.setParameter("roomsNumber", houseOfferEntity.getRoomsNumber());
            searchQuery = searchQuery.setParameter("surfaceArea", houseOfferEntity.getSurfaceArea());
            searchQuery = searchQuery.setParameter("constructionYear", houseOfferEntity.getConstructionYear());
            searchQuery.getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public List<HouseOfferEntity> findAll() {
        return entityManager.createQuery(GET_ALL_OFFERS_QUERY, HouseOfferEntity.class).getResultList();
    }
}
