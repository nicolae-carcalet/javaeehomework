package repositories;

import domain.JobEntity;
import domain.JobStatusEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

@Stateless
public class JobRepository {
    private static final String GET_ALL_JOBS_QUERY = "select j from JobEntity as j";

    @PersistenceContext(unitName = "DbPU")
    EntityManager entityManager;

    public void createJob(JobEntity jobEntity) {
        entityManager.persist(jobEntity);
    }

    public JobEntity findJobById(int jobId) {
        return entityManager.find(JobEntity.class, jobId);
    }

    public void updateStatus(int jobId, JobStatusEntity newStatus) {
        JobEntity jobEntity = findJobById(jobId);
        jobEntity.setStatus(newStatus);
    }

    public void updateFinished(int jobId, LocalDateTime finishedTime) {
        JobEntity jobEntity = findJobById(jobId);
        jobEntity.setEndedDateTime(finishedTime);
    }

    public List<JobEntity> findAll() {
        return entityManager.createQuery(GET_ALL_JOBS_QUERY, JobEntity.class).getResultList();
    }
}
