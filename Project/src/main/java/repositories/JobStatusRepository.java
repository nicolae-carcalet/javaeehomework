package repositories;

import domain.JobStatusEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class JobStatusRepository {
    private static final String GET_ALL_JOBS_QUERY = "select j from JobStatusEntity as j";

    @PersistenceContext(unitName = "DbPU")
    EntityManager entityManager;

    public List<JobStatusEntity> getAllEntities() {
        return entityManager.createQuery(GET_ALL_JOBS_QUERY, JobStatusEntity.class).getResultList();
    }
}
