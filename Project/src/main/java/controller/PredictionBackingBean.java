package controller;

import lombok.Data;
import model.CityNeighbourhoodMapping;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.Executor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Named("prediction")
@ApplicationScoped
public class PredictionBackingBean {
    private static final String PYTHON_TRAINING_SCRIPT_PATH = "C:\\Users\\i_dre\\PycharmProjects\\pythonProject3";
    private static final int TRAIN_JOB_SCRIPT_SUCCESS_RETURN_VALUE = 0;
    private static final String TRAIN_JOB_SCRIPT_TEMPLATE = "python %s";
    private static final String PYTHON_SCRIPT_NAME = "predict.py";
    private static final String PYTHON_SCRIPT_OUTPUT_FILE_NAME = "prediction.txt";
    private static final String CITY_NEIGHBOURHOOD_MAPPINGS_FILE_NAME = "ranks_mappings.txt";

    private int area;
    private int floorNumber;
    private int rooms;
    private int year;
    private int location;
    private double predictedPrice;
    private List<CityNeighbourhoodMapping> cityNeighbourhoodMappings;

    public void predictPrice() throws IOException {
        predictedPrice = getPricePrediction();
    }

    public List<CityNeighbourhoodMapping> getCityNeighbourhoodMappings() {
        List<String> cityNeighbourHoodRawFileMappings = readCityNeighbourhoodMappingsFileContent();
        return mapToCityNeighbourhood(cityNeighbourHoodRawFileMappings);
    }

    private List<CityNeighbourhoodMapping> mapToCityNeighbourhood(List<String> rawFileLines) {
        return rawFileLines.stream().map(this::extractCityNeighbourhoodDataFromRawFileLine).collect(Collectors.toList());
    }

    private CityNeighbourhoodMapping extractCityNeighbourhoodDataFromRawFileLine(String line) {
        String[] tokens = line.split(" ");
        String neighbourhoodName = getNeighbourhoodName(tokens);
        int assignedRank = Integer.parseInt(tokens[tokens.length - 1]);
        return new CityNeighbourhoodMapping(neighbourhoodName, assignedRank);
    }

    private String getNeighbourhoodName(String[] tokens) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0, len = tokens.length; i < len - 1; i++) {
            stringBuilder.append(tokens[i]).append(" ");
        }
        return stringBuilder.toString();
    }

    private List<String> readCityNeighbourhoodMappingsFileContent() {
        try {
            Path cityNeighbourhoodMappingsFilePath = Paths.get(PYTHON_TRAINING_SCRIPT_PATH, CITY_NEIGHBOURHOOD_MAPPINGS_FILE_NAME);
            return Files.readAllLines(cityNeighbourhoodMappingsFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private double getPricePrediction() throws IOException {
        String command = getPythonRunCommand();
        return runPythonPredictionScript(command);
    }

    private double runPythonPredictionScript(String command) throws IOException {
        CommandLine commandLine = CommandLine.parse(command);
        addCommandLineArguments(commandLine);
        executeScript(commandLine);
        return getScriptOutput();
    }

    private double executeScript(CommandLine commandLine) throws IOException {
        Executor defaultExecutor = getScriptExecutor();
        defaultExecutor.execute(commandLine);
        return getScriptOutput();
    }

    private double getScriptOutput() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(PYTHON_TRAINING_SCRIPT_PATH + File.separator + PYTHON_SCRIPT_OUTPUT_FILE_NAME));
        return Double.parseDouble(lines.get(0));
    }

    private Executor getScriptExecutor() {
        DefaultExecutor defaultExecutor = new DefaultExecutor();
        defaultExecutor.setExitValue(TRAIN_JOB_SCRIPT_SUCCESS_RETURN_VALUE);
        return defaultExecutor;
    }

    private void addCommandLineArguments(CommandLine commandLine) {
        commandLine.addArgument(String.valueOf(year));
        commandLine.addArgument(String.valueOf(rooms));
        commandLine.addArgument(String.valueOf(floorNumber));
        commandLine.addArgument(String.valueOf(area));
        commandLine.addArgument(String.valueOf(location));
    }

    private String getPythonRunCommand() {
        String scriptPath = PYTHON_TRAINING_SCRIPT_PATH + File.separator + PYTHON_SCRIPT_NAME;
        return String.format(TRAIN_JOB_SCRIPT_TEMPLATE, scriptPath);
    }
}
