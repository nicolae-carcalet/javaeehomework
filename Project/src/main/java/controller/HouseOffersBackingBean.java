package controller;

import domain.HouseOfferEntity;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import repositories.HouseOfferRepository;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@Named("houseOffersBackingBean")
@ApplicationScoped
public class HouseOffersBackingBean {
    public static final String CSV_MEDIA_TYPE = "text/csv";
    private static final String PYTHON_TRAINING_SCRIPT_PATH = "C:\\Users\\i_dre\\PycharmProjects\\pythonProject3";
    private static final String HOUSE_OFFERS_CSV_FILENAME = "data.csv";
    @EJB
    private HouseOfferRepository houseOfferRepository;

    private List<HouseOfferEntity> houseOffers;

    public HouseOfferRepository getHouseOfferRepository() {
        return houseOfferRepository;
    }

    public List<HouseOfferEntity> getHouseOffers() {
        houseOffers = houseOfferRepository.findAll();
        return houseOffers;
    }

    public StreamedContent getCsvFile() {
        return DefaultStreamedContent.builder()
                .name(HOUSE_OFFERS_CSV_FILENAME)
                .contentType(CSV_MEDIA_TYPE)
                .stream(() -> {
                    try {
                        return new FileInputStream(PYTHON_TRAINING_SCRIPT_PATH + File.separator + HOUSE_OFFERS_CSV_FILENAME);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .build();
    }
}