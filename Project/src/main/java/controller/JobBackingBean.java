package controller;

import domain.JobEntity;
import domain.JobStatusEntity;
import domain.JobTypeEntity;
import lombok.Data;
import repositories.JobRepository;
import repositories.JobStatusRepository;
import repositories.JobTypeRepository;
import service.SchedulingService;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Named("jobsBackingBean")
@ApplicationScoped
public class JobBackingBean {

    private static final int DEFAULT_DELAY_FOR_TRAINING_JOB = 0;
    private static final String SCRAPE_JOB_NAME = "Scrape";

    private String jobType;
    private String delay;
    private String city;

    @EJB
    private JobRepository jobRepository;

    @EJB
    private JobStatusRepository jobStatusRepository;

    @EJB
    private JobTypeRepository jobTypeRepository;

    @EJB
    private SchedulingService schedulingService;

    private List<JobEntity> jobEntities;
    private List<JobStatusEntity> jobStatusEntities;
    private List<JobTypeEntity> jobTypeEntities;

    public void addNewJob() {
        JobEntity job = createJobEntity();
        jobRepository.createJob(job);
        schedulingService.addJob(job);
    }

    public List<JobEntity> getJobEntities() {
        return jobRepository.findAll();
    }

    private JobEntity createJobEntity() {
        LocalDateTime now = LocalDateTime.now();
        jobStatusEntities = jobStatusRepository.getAllEntities();
        jobTypeEntities = jobTypeRepository.getAllEntities();
        JobStatusEntity queuedStatus = jobStatusEntities.get(0);
        JobTypeEntity jobTypeManagedEntity = jobType.equals(SCRAPE_JOB_NAME) ? jobTypeEntities.get(0) : jobTypeEntities.get(1);
        int parsedDelay = jobType.equals(SCRAPE_JOB_NAME) ? Integer.parseInt(delay) : DEFAULT_DELAY_FOR_TRAINING_JOB;
        return new JobEntity(null, now, null, queuedStatus, jobTypeManagedEntity, parsedDelay);
    }
}