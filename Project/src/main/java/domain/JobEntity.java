package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "jobs")
@AllArgsConstructor
@NoArgsConstructor
public class JobEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id")
    private Integer id;

    @Column(name = "started")
    private LocalDateTime startedDateTime;

    @Column(name = "finished")
    private LocalDateTime endedDateTime;

    @OneToOne
    @JoinColumn(name = "job_status", referencedColumnName = "job_status_id")
    private JobStatusEntity status;

    @OneToOne
    @JoinColumn(name = "job_type", referencedColumnName = "job_type_id")
    private JobTypeEntity type;

    private Integer delay;
}
