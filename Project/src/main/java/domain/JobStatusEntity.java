package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "job_status")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobStatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_status_id")
    private Integer id;

    @Column(name = "status_name")
    private String status;
}
