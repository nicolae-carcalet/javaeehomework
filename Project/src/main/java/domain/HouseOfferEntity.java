package domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "house_prices")
public class HouseOfferEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @NotNull
    private int price;

    @NotNull
    private String location;

    @Column(name = "floor_number")
    @NotNull
    private int floorNumber;

    @Column(name = "rooms")
    @NotNull
    private int roomsNumber;

    @Column(name = "surface_sq_meters")
    @NotNull
    private int surfaceArea;

    @Column(name = "construction_year")
    @NotNull
    private int constructionYear;
}
