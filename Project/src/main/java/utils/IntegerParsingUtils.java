package utils;

public class IntegerParsingUtils {
    public static int parseInt(String value) {
        int number = 0;
        boolean inNumber = false;

        for (int len = value.length(), i = 0; i < len; i++) {
            if (!Character.isDigit(value.charAt(i)) && inNumber) {
                return number;
            }
            if (Character.isDigit(value.charAt(i))) {
                inNumber = true;
                number = number * 10 + Character.getNumericValue(value.charAt(i));
            }
        }

        return number;
    }
}
