package utils;

import domain.HouseOfferEntity;
import model.CityNeighbourhoodMapping;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileUtils {

    public static void dumpHouseOffersToCsv(String csvPath, List<HouseOfferEntity> items) {
        try (BufferedWriter outputFile = Files.newBufferedWriter(Paths.get(csvPath))) {
            CSVPrinter csvPrinter = new CSVPrinter(outputFile, CSVFormat.DEFAULT
                    .withHeader("ID", "Price", "Construction Year", "Rooms Number", "Floor Number", "Location", "Surface Area"));
            writeHouseOffersToOutputFile(csvPrinter, items);
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeHouseOffersToOutputFile(CSVPrinter csvPrinter, List<HouseOfferEntity> houseOffers) throws IOException {
        for (HouseOfferEntity houseOfferEntity : houseOffers) {
            writeHouseOfferToOutputFile(csvPrinter, houseOfferEntity);
        }
    }

    private static void writeHouseOfferToOutputFile(CSVPrinter csvPrinter, HouseOfferEntity houseOfferEntity) throws IOException {
        String id = houseOfferEntity.getId();
        String location = houseOfferEntity.getLocation();
        int constructionYear = houseOfferEntity.getConstructionYear();
        int roomsNumber = houseOfferEntity.getRoomsNumber();
        int floorNumber = houseOfferEntity.getFloorNumber();
        int price = houseOfferEntity.getPrice();
        int surfaceArea = houseOfferEntity.getSurfaceArea();
        csvPrinter.printRecord(id, price, constructionYear, roomsNumber, floorNumber, location, surfaceArea);
    }
}
