package service;

import constants.HouseOffersSiteBaseUrl;
import controller.JobBackingBean;
import domain.HouseOfferEntity;
import domain.JobEntity;
import domain.JobStatusEntity;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repositories.HouseOfferRepository;
import repositories.JobRepository;
import repositories.JobStatusRepository;
import scraper.HouseOfferSiteScraper;
import scraper.ImobiliareROExtractor;
import utils.FileUtils;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Stateful
@Startup
public class SchedulingService {
    private static final Logger LOG = LoggerFactory.getLogger(JobBackingBean.class);
    private static final int SCHEDULER_THREAD_POOL_SIZE = 1;
    private static final int SCHEDULER_BETWEEN_TASKS_DELAY = 5;
    private static final String PYTHON_TRAINING_SCRIPT_PATH = "C:\\Users\\i_dre\\PycharmProjects\\pythonProject3";
    private static final String PYTHON_SCRIPT_NAME = "main.py";
    private static final String HOUSE_OFFERS_CSV_FILENAME = "data.csv";
    private static final String TRAIN_JOB_SCRIPT_TEMPLATE = "python %s";
    private static final int TRAIN_JOB_SCRIPT_SUCCESS_RETURN_VALUE = 0;
    private static final String SCRAPE_JOB_NAME = "Scrape";
    private static Queue<JobEntity> jobsQueue;
    private static ScheduledExecutorService scheduler;
    private static HouseOfferSiteScraper scraper;

    @EJB
    private JobRepository jobRepository;

    @EJB
    private JobStatusRepository jobStatusRepository;

    @EJB
    private HouseOfferRepository houseOfferRepository;

    private List<JobStatusEntity> jobStatusEntities;

    @PostConstruct
    public void init() {
        jobsQueue = new LinkedList<>();
        scheduler = Executors.newScheduledThreadPool(SCHEDULER_THREAD_POOL_SIZE);
        scraper = new ImobiliareROExtractor();
        jobStatusEntities = jobStatusRepository.getAllEntities();

        Runnable runNewJobTask = () -> {
            JobEntity jobEntity = jobsQueue.poll();

            if (jobEntity == null) {
                return;
            }

            int jobId = jobEntity.getId();
            int delay = jobEntity.getDelay();

            jobRepository.updateStatus(jobId, jobStatusEntities.get(1));

            try {
                if (isScrapingJob(jobEntity)) {
                    LOG.info("Starting scraping job with id {} and delay {}", jobId, delay);
                    scrapeJob(delay, HouseOffersSiteBaseUrl.IMOBILIARE_RO.getBaseUrl());
                } else {
                    LOG.info("Starting training job with id {}", jobId);
                    trainJob();
                }
                jobRepository.updateStatus(jobId, jobStatusEntities.get(2));
                jobRepository.updateFinished(jobId, LocalDateTime.now());
            } catch (Exception e) {
                jobRepository.updateStatus(jobId, jobStatusEntities.get(3));
            }
        };
        scheduler.scheduleWithFixedDelay(runNewJobTask, 1, SCHEDULER_BETWEEN_TASKS_DELAY, TimeUnit.SECONDS);
    }

    private void trainJob() throws IOException {
        try {
            createTrainingDatasetFile();
            String command = getPythonRunCommand();
            int statusCode = runPythonTrainScript(command);
            LOG.info("Training ended with return code {}", statusCode);
        } catch (IOException e) {
            LOG.error("Exception thrown when training: {}", e.getMessage());
            LOG.error("Stack trace: {}", ExceptionUtils.getStackTrace(e));
            throw e;
        }
    }

    private void createTrainingDatasetFile() {
        String csvPath = PYTHON_TRAINING_SCRIPT_PATH + File.separator + HOUSE_OFFERS_CSV_FILENAME;
        List<HouseOfferEntity> houseOffers = houseOfferRepository.findAll();
        FileUtils.dumpHouseOffersToCsv(csvPath, houseOffers);
    }

    private int runPythonTrainScript(String command) throws IOException {
        CommandLine commandLine = CommandLine.parse(command);
        DefaultExecutor defaultExecutor = new DefaultExecutor();
        defaultExecutor.setExitValue(TRAIN_JOB_SCRIPT_SUCCESS_RETURN_VALUE);
        return defaultExecutor.execute(commandLine);
    }

    private String getPythonRunCommand() {
        String scriptPath = PYTHON_TRAINING_SCRIPT_PATH + File.separator + PYTHON_SCRIPT_NAME;
        return String.format(TRAIN_JOB_SCRIPT_TEMPLATE, scriptPath);
    }

    private boolean isScrapingJob(JobEntity jobEntity) {
        return jobEntity.getType().getType().equals(SCRAPE_JOB_NAME);
    }

    public void addJob(JobEntity jobEntity) {
        jobsQueue.add(jobEntity);
    }

    private void scrapeJob(int delay, String baseUrl) throws IOException, InterruptedException {
        try {
            int pagesCount = scraper.getOfferPagesCount(baseUrl);
            for (int i = 1; i <= pagesCount; i++) {
                LOG.info("Scraping page {} out of {}", i, pagesCount);
                String currentPageUrl = scraper.getPageUrlForPageNumber(baseUrl, i);
                Set<HouseOfferEntity> houseOfferEntities = scraper.getHouseOffers(currentPageUrl);
                removeDuplicates(houseOfferEntities);
                houseOfferRepository.addAll(houseOfferEntities);
                Thread.sleep(delay * 1000);
            }
        } catch (Exception e) {
            LOG.error("Exception thrown when scraping information: {}", e.getMessage());
            LOG.error("Stack trace: {}", ExceptionUtils.getStackTrace(e));
            throw e;
        }
    }

    private void removeDuplicates(Set<HouseOfferEntity> houseOfferEntities) {
        Set<HouseOfferEntity> duplicates = houseOfferEntities
                .stream().filter(houseOfferEntity -> houseOfferRepository.houseOfferExists(houseOfferEntity))
                .collect(Collectors.toSet());
        houseOfferEntities.removeAll(duplicates);
    }
}
