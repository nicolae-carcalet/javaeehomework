package constants;

public enum HouseOffersSiteBaseUrl {
    IMOBILIARE_RO("https://www.imobiliare.ro/inchirieri-apartamente/iasi");

    private String baseUrl;

    HouseOffersSiteBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
