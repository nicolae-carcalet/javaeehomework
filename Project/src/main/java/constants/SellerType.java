package constants;

public enum SellerType {
    AGENCY,
    PERSON
}
