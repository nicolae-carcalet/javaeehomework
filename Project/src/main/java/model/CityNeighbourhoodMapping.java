package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CityNeighbourhoodMapping {
    private String neighbourhoodName;
    private int rank;
}
