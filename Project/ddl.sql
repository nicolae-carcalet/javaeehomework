create table house_prices(id serial primary key,
post_title varchar(1000), post_description text,
price int, surface_sq_meters int,
rooms int, construction_year int, floor_number int);