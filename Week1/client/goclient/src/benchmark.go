package main

import (
	"container/list"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
	"fmt"
)

type RequestMetadata struct {
	ServeTime             time.Duration
	ResponseContentLength int64
}

var mutex sync.Mutex
var benchmarkMetadata *list.List = list.New()
var goroutinesPool sync.WaitGroup

func benchmark() {
	makeConcurrentCalls()
}

func showBenchmarkSummary() {
	for e := benchmarkMetadata.Front(); e != nil; e = e.Next() {
		fmt.Printf("%v\n", e.Value)
	}
}

func makeConcurrentCalls() {
	for i := 0; i < REQUEST_COUNT; i++ {
		applicationURL := getRemoteApplicationURL()
		go benchmarkGetRequest(applicationURL)
		goroutinesPool.Add(1)
	}
	goroutinesPool.Wait()
}

func getRemoteApplicationURL() string {
	letters := generateRandomLetters()
	return HEROKU_DEPLOYED_URL + letters
}

func generateRandomLetters() string {
	stringLength := rand.Intn(MAX_LETTERS_STRING_LENGTH)
	var stringBuilder strings.Builder
	for i := 0; i < stringLength; i++ {
		stringBuilder.WriteRune(letterRunes[rand.Intn(len(letterRunes))])
	}
	return stringBuilder.String()
}

func benchmarkGetRequest(applicationURL string) {
	resp, requestServeTime, err := makeTimedGetRequest(applicationURL)
	if err != nil {
		println("GET request failed")
		println(err.Error())
		return
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		storeRequestMetadata(resp, requestServeTime)
	}
	goroutinesPool.Done()
}

func storeRequestMetadata(response *http.Response, timeDuration time.Duration) {
	requestMetadata := RequestMetadata{ServeTime: timeDuration,
		ResponseContentLength: response.ContentLength}
	mutex.Lock()
	benchmarkMetadata.PushBack(requestMetadata)
	mutex.Unlock()
}

func makeTimedGetRequest(applicationURL string) (response *http.Response, elapsed time.Duration, err error) {
	t1 := time.Now()
	resp, err := http.Get(applicationURL)
	t2 := time.Now()
	timeDifference := t2.Sub(t1)
	return resp, timeDifference, err
}
