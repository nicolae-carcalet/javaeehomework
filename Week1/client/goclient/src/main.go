package main

import (
	"io/ioutil"
	"net/http"
)

var debug_mode bool = true

func main() {
	if debug_mode {
		applicationURL := getDebugApplicationURL()
		makeGetRequestToServlet(applicationURL)
	} else {
		benchmark()
		showBenchmarkSummary()
	}
}

func getDebugApplicationURL() string {
	baseRequestURL := SERVLET_HOST + ":" + SERVLET_PORT + "/" + APPLICATION_NAME + "/" + SERVLET_ENDPOINT
	return baseRequestURL + "?simple=true&letters=aajv"
}

func makeGetRequestToServlet(url string) {
	resp, err := http.Get(url)

	if err != nil {
		println("GET request failed")
		println(err.Error())
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		responseBodyBytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			println("Failed to read response body")
			return
		}
		bodyString := string(responseBodyBytes)
		println(bodyString)
	}
}
