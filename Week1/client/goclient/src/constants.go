package main

var letterRunes []rune = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

const (
	HEROKU_DEPLOYED_URL       = "https://desolate-gorge-96172.herokuapp.com/word?simple=true&letters="
	REQUEST_COUNT             = 1000
	MAX_LETTERS_STRING_LENGTH = 20
)

const (
	SERVLET_HOST     = "http://127.0.0.1"
	SERVLET_ENDPOINT = "word"
	APPLICATION_NAME = "Server_war"
	SERVLET_PORT     = "8080"
)
