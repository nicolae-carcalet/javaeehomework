import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet(name = "Word Servlet", urlPatterns = {"/word"})
public class WordServlet extends HttpServlet {
    private static final String WORDS_FILE_PATH = "words.txt";
    private static final String LETTERS_PARAMETER_NAME = "letters";
    private static final String USER_AGENT_HEADER = "User-Agent";
    private static final String PARAMETERS_LOG_SEPARATOR = "&";
    private static final String SIMPLE_MODE_PARAMETER = "simple";
    private List<String> words;

    @Override
    public void init() throws ServletException {
        try {
            words = new ArrayList<>();
            initWordsDictionary();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initWordsDictionary() throws IOException {
        BufferedReader resourceFileInputStream = getResourceFileInputStream();
        readWordsFromFile(resourceFileInputStream);
    }

    private BufferedReader getResourceFileInputStream() throws FileNotFoundException {
        String realPathToWordsFile = getRealPathToWordsFile();
        return openResourceFile(realPathToWordsFile);
    }

    private BufferedReader openResourceFile(String realPathToWordsFile) throws FileNotFoundException {
        FileReader fileReader = new FileReader(realPathToWordsFile);
        return new BufferedReader(fileReader);
    }

    private String getRealPathToWordsFile() {
        ServletContext servletContext = getServletContext();
        return servletContext.getRealPath(WORDS_FILE_PATH);
    }

    private void readWordsFromFile(BufferedReader resourceFileInputStream) throws IOException {
        boolean eof = false;
        while (!eof) {
            String currentLine = resourceFileInputStream.readLine();
            if (currentLine == null) {
                eof = true;
            } else {
                words.add(cleanUpWord(currentLine));
            }
        }
    }

    private String cleanUpWord(String rawWord) {
        return rawWord.trim();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String letters = req.getParameter(LETTERS_PARAMETER_NAME);
        List<String> matchingWords = getPossibleWords(letters);
        logRequestInformation(req);

        if (req.getParameter(SIMPLE_MODE_PARAMETER) == null) {
            resp.getWriter().println(buildHTMLResponseOutput(matchingWords));
        } else {
            resp.getWriter().println(buildSimpleResponseOutput(matchingWords));
        }
    }

    private void logRequestInformation(HttpServletRequest request) {
        ServletContext servletContext = getServletContext();
        servletContext.log(String.format("Client's IP: %s", request.getRemoteAddr()));
        servletContext.log(String.format("Request method: %s", request.getMethod()));
        servletContext.log(String.format("User agent: %s", request.getHeader(USER_AGENT_HEADER)));
        servletContext.log(String.format("Client's language: %s", request.getLocale().getLanguage()));
        servletContext.log(String.format("Request parameters: %s", getRequestParametersAsString(request)));
    }

    private String getRequestParametersAsString(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, String[]> items : request.getParameterMap().entrySet()) {
            stringBuilder.append(items.getKey()).append("=");
            stringBuilder.append(Arrays.toString(items.getValue()));
            stringBuilder.append(PARAMETERS_LOG_SEPARATOR);
        }

        return stringBuilder.toString();
    }

    private String buildHTMLResponseOutput(List<String> matchingWords) {
        StringBuilder stringBuilder = new StringBuilder();
        addHTMLStartingBoilerplate(stringBuilder);
        addWordsAsHTMLList(stringBuilder, matchingWords);
        addHTMLEndBoilerplate(stringBuilder);
        return stringBuilder.toString();
    }

    private String buildSimpleResponseOutput(List<String> matchingWords) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String word : matchingWords) {
            stringBuilder.append(word).append("\n");
        }

        return stringBuilder.toString();
    }

    private void addHTMLStartingBoilerplate(StringBuilder stringBuilder) {
        stringBuilder.append("<html>");
        stringBuilder.append("<body>");
    }

    private void addHTMLEndBoilerplate(StringBuilder stringBuilder) {
        stringBuilder.append("</body>");
        stringBuilder.append("</html>");
    }

    private void addWordsAsHTMLList(StringBuilder stringBuilder, List<String> matchingWords) {
        stringBuilder.append("<ul>");
        for (String word : matchingWords) {
            stringBuilder.append("<li>").append(word).append("</li>");
        }
        stringBuilder.append("</ul>");
    }

    private List<String> getPossibleWords(String letters) {
        Map<Character, Integer> lettersHashMap = transformLettersStringIntoHashMap(letters);
        Stream<String> possibleWords = words.parallelStream().filter(word -> canBeFormedWithLetters(word, lettersHashMap));
        return possibleWords.collect(Collectors.toList());
    }

    private Map<Character, Integer> transformLettersStringIntoHashMap(String letters) {
        Map<Character, Integer> lettersLookup = new HashMap<>();

        final int lettersCount = letters.length();
        for (int i = 0; i < lettersCount; i++) {
            char currentLetter = letters.charAt(i);
            lettersLookup.put(currentLetter, lettersLookup.getOrDefault(currentLetter, 0) + 1);
        }
        return lettersLookup;
    }

    private boolean canBeFormedWithLetters(String word, Map<Character, Integer> lettersLookup) {
        Map<Character, Integer> currentWordLetterFrequencies = new HashMap<>();
        final int wordLength = word.length();

        for (int i = 0; i < wordLength; i++) {
            char currentLetter = word.charAt(i);
            int currentLetterFrequency = currentWordLetterFrequencies.getOrDefault(currentLetter, 0) + 1;

            if (!lettersLookup.containsKey(currentLetter) || currentLetterFrequency > lettersLookup.get(currentLetter)) {
                return false;
            }

            currentWordLetterFrequencies.put(currentLetter, currentLetterFrequency);
        }
        return true;
    }
}
