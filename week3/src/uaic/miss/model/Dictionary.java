package uaic.miss.model;

import java.util.*;

public class Dictionary implements Iterable<DictionaryEntry> {
    private static Dictionary instance = new Dictionary();
    private static Set<DictionaryEntry> dictionaryEntries;

    private Dictionary() {
        dictionaryEntries = new HashSet<>();
    }

    public static Dictionary newInstance() {
        return instance;
    }

    public synchronized void addDictionaryEntry(DictionaryEntry dictionaryEntry) {
        if (dictionaryEntry != null) {
            dictionaryEntries.add(dictionaryEntry);
        }
    }

    public List<DictionaryEntry> getDefinitions(String key) {
        List<DictionaryEntry> found = new ArrayList<>();

        for (DictionaryEntry entry : dictionaryEntries) {
            if (entry.getWord().equals(key)) {
                found.add(entry);
            }
        }

        return found;
    }

    public boolean isEntryAlreadyPresent(String word, String definition, String language, Date insertionDate) {
        return dictionaryEntries.contains(new DictionaryEntry(word, definition, language, insertionDate));
    }

    // NOTE: For JSP custom tag(the regular iterator can't be used)
    public Set<DictionaryEntry> getEntries() {
        return dictionaryEntries;
    }

    @Override
    public Iterator<DictionaryEntry> iterator() {
        return dictionaryEntries.iterator();
    }
}
