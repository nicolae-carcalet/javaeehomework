package uaic.miss.model;

public class CaptchaEquation {
    private int x;
    private int y;
    private String operation;

    public CaptchaEquation(int x, int y, String operation) {
        this.x = x;
        this.y = y;
        this.operation = operation;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int evaluateExpession() {
        switch (operation) {
            case "+":
                return x + y;
            case "-":
                return x - y;
            case "*":
                return x * y;
        }
        return Integer.MAX_VALUE;
    }
}
