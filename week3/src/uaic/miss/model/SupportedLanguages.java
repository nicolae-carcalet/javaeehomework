package uaic.miss.model;

import java.util.*;

public class SupportedLanguages {
    private final static List<String> DEFAULT_SUPPORTED_LANGUAGES = List.of("English", "Deutsch");
    private List<String> supportedLanguages;

    public SupportedLanguages() {
        supportedLanguages = new ArrayList<>();
        addInitialSupportedLanguages();
    }

    private void addInitialSupportedLanguages() {
        supportedLanguages.addAll(DEFAULT_SUPPORTED_LANGUAGES);
    }

    public List<String> getAllSupportedLanguages() {
        return supportedLanguages;
    }
}
