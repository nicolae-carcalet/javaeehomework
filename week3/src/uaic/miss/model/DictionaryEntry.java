package uaic.miss.model;

import java.util.Date;

public class DictionaryEntry {
    private String word;
    private String definition;
    private String language;
    private Date insertionDate;

    public DictionaryEntry(String word, String definition, String language, Date insertionDate) {
        super();
        this.word = word;
        this.definition = definition;
        this.language = language;
        this.insertionDate = insertionDate;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getInsertionDate() {
        return insertionDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((definition == null) ? 0 : definition.hashCode());
        result = prime * result + ((insertionDate == null) ? 0 : insertionDate.hashCode());
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((word == null) ? 0 : word.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DictionaryEntry other = (DictionaryEntry) obj;
        if (definition == null) {
            if (other.definition != null)
                return false;
        } else if (!definition.equals(other.definition))
            return false;
        if (insertionDate == null) {
            if (other.insertionDate != null)
                return false;
        } else if (!insertionDate.equals(other.insertionDate))
            return false;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (word == null) {
            if (other.word != null)
                return false;
        } else if (!word.equals(other.word))
            return false;
        return true;
    }
}
