package uaic.miss.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import uaic.miss.model.Dictionary;
import uaic.miss.model.DictionaryEntry;

public class DefinitionTag extends SimpleTagSupport {
    private String word;
    private String language;
    private Dictionary dictionary;

    public DefinitionTag() {
        dictionary = Dictionary.newInstance();
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter jspWriter = getJspContext().getOut();
        displayWordDefinitions(jspWriter);
    }

    private void displayWordDefinitions(JspWriter jspWriter) throws IOException {
        List<DictionaryEntry> foundDefinitions = dictionary.getDefinitions(word);
        jspWriter.write("<ul>");
        for (DictionaryEntry entry : foundDefinitions) {
            if (language != null && entry.getWord().equals(language)) {
                continue;
            }
            jspWriter.write("<li>");
            jspWriter.write(String.format("%s in %s", entry.getDefinition(), entry.getLanguage()));
            jspWriter.write("</li>");
        }
        jspWriter.write("</ul>");
    }
}
