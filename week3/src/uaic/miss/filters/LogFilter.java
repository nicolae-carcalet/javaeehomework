package uaic.miss.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(filterName = "logFilter", urlPatterns = {"/index.html", "/input.jsp", "/result.jsp"})
public class LogFilter implements Filter {
    private static final String LOG_FORMAT = "Request to %s from %s";

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        logRequest(req);
        chain.doFilter(req, resp);
    }

    private void logRequest(ServletRequest req) {
        ServletContext context = req.getServletContext();
        String remoteHost = req.getRemoteHost();
        String contextPath = ((HttpServletRequest) req).getServletPath();
        context.log(String.format(LOG_FORMAT, contextPath, remoteHost));
    }
}
