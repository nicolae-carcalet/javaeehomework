package uaic.miss.filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import uaic.miss.filters.wrapper.CustomHttpServletResponseWrapper;

@WebFilter(filterName = "decorator", urlPatterns = {"/*"})
public class DecoratorFilter implements Filter {
    private static final String FOOTER_PARENT_ELEMENT_TAG = "body";
    private static final String FOOTER_ELEMENT_TAG = "p";
    private static final String FOOTER_ELEMENT_HTML = "Footer added with Jsoup";
    private Element footerElement;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
        this.footerElement = createFooterElement();
    }

    private Element createFooterElement() {
        Element footerElement = new Element(FOOTER_ELEMENT_TAG);
        footerElement.append(FOOTER_ELEMENT_HTML);
        return footerElement;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        CustomHttpServletResponseWrapper responseWrapper = new CustomHttpServletResponseWrapper(resp);
        chain.doFilter(req, responseWrapper);

        String responseHTLM = getResponseHTML(responseWrapper);
        responseHTLM = addFooter(responseHTLM);
        addWrapperResponseToActualResponse(resp, responseHTLM);
    }

    private String addFooter(String responseHTML) {
        Document document = Jsoup.parse(responseHTML);
        Elements parentElements = getFooterParentElements(document);
        parentElements.after(footerElement.toString());
        return document.toString();
    }

    private Elements getFooterParentElements(Document document) {
        return document.getElementsByTag(FOOTER_PARENT_ELEMENT_TAG);
    }

    private void addWrapperResponseToActualResponse(ServletResponse resp, String wrapperResponse) throws IOException {
        resp.getOutputStream().println(wrapperResponse);
    }

    private String getResponseHTML(CustomHttpServletResponseWrapper responseWrapper) {
        return responseWrapper.toString();
    }
}
