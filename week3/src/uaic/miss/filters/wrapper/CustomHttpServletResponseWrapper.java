package uaic.miss.filters.wrapper;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class CustomHttpServletResponseWrapper extends HttpServletResponseWrapper {

    private StringWriter sw = new StringWriter();

    public CustomHttpServletResponseWrapper(int bufferSize, HttpServletResponse response) {
        super(response);
    }

    public CustomHttpServletResponseWrapper(ServletResponse resp) {
        super((HttpServletResponse) resp);
    }

    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(sw);
    }

    public String toString() {
        return sw.toString();
    }
}
