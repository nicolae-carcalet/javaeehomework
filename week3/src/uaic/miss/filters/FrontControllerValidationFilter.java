package uaic.miss.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "FrontControllerValidationFilter", urlPatterns = {"/add"})
public class FrontControllerValidationFilter implements Filter {
    private static final String INPUT_JSP_FILENAME = "/input.jsp";
    private static final String WORD_REQUEST_PARAMETER = "word";
    private static final String DEFINITION_REQUEST_PARAMETER = "definition";
    private static final String LANGUAGE_REQUEST_PARAMETER = "language";
    private static final String DEFAULT_LANGUAGE_CONTEXT_PARAMETER = "defaultLanguage";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (isInvalidRequest(request)) {
            redirectBackToInputPage(request, response);
            return;
        } else {
            if (!hasLanguageParameterValue(request)) {
                request = addDefaultLanguage(request);
            }
        }
        chain.doFilter(request, response);
    }

    private HttpServletRequest addDefaultLanguage(ServletRequest request) {
        HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper((HttpServletRequest) request) {
            @Override
            public String getParameter(String name) {
                if (!name.equals(LANGUAGE_REQUEST_PARAMETER)) {
                    return super.getParameter(name);
                }
                return request.getServletContext().getInitParameter(DEFAULT_LANGUAGE_CONTEXT_PARAMETER);
            }
        };
        return wrapper;
    }

    private boolean hasLanguageParameterValue(ServletRequest request) {
        return request.getParameter(LANGUAGE_REQUEST_PARAMETER) != null;
    }

    private void redirectBackToInputPage(ServletRequest request, ServletResponse response) throws IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String redirectPath = httpRequest.getContextPath() + INPUT_JSP_FILENAME;
        httpResponse.sendRedirect(redirectPath);
    }

    private boolean isInvalidRequest(ServletRequest request) {
        return request.getParameter(WORD_REQUEST_PARAMETER) == null || request.getParameter(DEFINITION_REQUEST_PARAMETER) == null;
    }
}
