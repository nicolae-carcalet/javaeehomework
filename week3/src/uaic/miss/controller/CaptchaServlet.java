package uaic.miss.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uaic.miss.model.CaptchaEquation;

@WebServlet(name = "/CaptchaServlet", urlPatterns = {"/captcha"})
public class CaptchaServlet extends HttpServlet {
    private static final String CAPTCHA_RESPONSE_CONTENT_TYPE = "image/jpg";
    private static final int CAPTCHA_IMAGE_WIDTH = 150;
    private static final int CAPTCHA_IMAGE_HEIGHT = 100;
    private static final String FONT_NAME = "Arial";
    private static final int FONT_SIZE = 30;
    private static final Color FONT_COLOR = Color.WHITE;
    private static final int NUMBER_UPPER_BOUND = 10;
    private static final List<String> SUPPORTED_OPERATIONS = List.of("+", "-", "*");
    private static final int FIRST_OPERAND_X = 30;
    private static final int SECOND_OPERAND_X = 70;
    private static final int OPERATOR_X = 50;
    private static final int EQUATION_TOKEN_Y = 40;
    private static final String CAPTCHA_COOKIE_NAME = "captchaResult";
    private Font captchaFont;
    private Random random;

    @Override
    public void init() throws ServletException {
        random = new Random();
        random.setSeed(System.currentTimeMillis());
        captchaFont = new Font(FONT_NAME, Font.BOLD, FONT_SIZE);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CAPTCHA_RESPONSE_CONTENT_TYPE);
        BufferedImage captchaImage = new BufferedImage(CAPTCHA_IMAGE_WIDTH, CAPTCHA_IMAGE_HEIGHT,
                BufferedImage.TYPE_INT_RGB);
        CaptchaEquation captchaEquation = getCaptchaEquation();
        drawCaptchaEquation(captchaImage, captchaEquation);
        addCaptchaEquationCookie(response, captchaEquation);
        writeCaptchaImageIntoResponse(captchaImage, response);
    }

    private void addCaptchaEquationCookie(HttpServletResponse response, CaptchaEquation captchaEquation) {
        int captchaResult = captchaEquation.evaluateExpession();
        Cookie cookie = new Cookie(CAPTCHA_COOKIE_NAME, String.valueOf(captchaResult));
        response.addCookie(cookie);
    }

    private CaptchaEquation getCaptchaEquation() {
        int x = random.nextInt(NUMBER_UPPER_BOUND);
        int y = random.nextInt(NUMBER_UPPER_BOUND);
        String operation = getRandomOperation();
        return new CaptchaEquation(x, y, operation);
    }

    private void writeCaptchaImageIntoResponse(BufferedImage captchaImage, HttpServletResponse response)
            throws IOException {
        OutputStream osImage = response.getOutputStream();
        ImageIO.write(captchaImage, "jpeg", osImage);
    }

    private void drawCaptchaEquation(BufferedImage captchaImage, CaptchaEquation captchaEquation) {
        Graphics graphicsContext = captchaImage.createGraphics();
        initGraphicsContext(graphicsContext);
        drawEquation(graphicsContext, captchaEquation);
        graphicsContext.dispose();
    }

    private void drawEquation(Graphics graphicsContext, CaptchaEquation captchaEquation) {
        graphicsContext.drawString(String.valueOf(captchaEquation.getX()), FIRST_OPERAND_X, EQUATION_TOKEN_Y);
        graphicsContext.drawString(captchaEquation.getOperation(), OPERATOR_X, EQUATION_TOKEN_Y);
        graphicsContext.drawString(String.valueOf(captchaEquation.getY()), SECOND_OPERAND_X, EQUATION_TOKEN_Y);
    }

    private void initGraphicsContext(Graphics graphicsContext) {
        graphicsContext.setColor(FONT_COLOR);
        graphicsContext.setFont(captchaFont);
    }

    private String getRandomOperation() {
        int randIndex = random.nextInt(SUPPORTED_OPERATIONS.size());
        return SUPPORTED_OPERATIONS.get(randIndex);
    }
}
