<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>

<fmt:setBundle basename="messages"/>
<fmt:setLocale value="${request.getLocale()}" scope="page"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error page</title>
</head>
<body>
<p><fmt:message key="ERROR_MESSAGE"/>.</p>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
    out.write("<p>" + errorMessage + "</p>");
%>
</body>
</html>
