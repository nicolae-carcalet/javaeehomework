<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="uaic.miss.model.*" %>
<%@page import="java.io.*,java.util.Locale" %>
<%@taglib prefix="my" uri="WEB-INF/custom.tld" %>
<%@taglib prefix="mystuff" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setBundle basename="messages"/>
<fmt:setLocale value="${request.getLocale()}" scope="page"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        <fmt:message key="RESULT_TITLE_MESSAGE"/>
    </title>
</head>
<body>
<fmt:message key="RESULT_GREETING_MESSAGE"/>
<% String insertedWord = (String) request.getAttribute("insertedWord"); %>
<% String language = (String) request.getAttribute("language"); %>
<% Dictionary dictionary = (Dictionary) request.getAttribute("dictionary"); %>

<p><fmt:message key="RESULT_SIMILAR_WORDS_MESSAGE"/></p>
<my:definition word="${insertedWord}" language="${language}"/>
<mystuff:viewDictionary dictionary="${dictionary}" language="${language}"></mystuff:viewDictionary>
</body>
</html>
