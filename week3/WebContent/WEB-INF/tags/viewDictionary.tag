<%@ tag language="java" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="language" required="false" type="java.lang.String" description="Language." %>
<%@ attribute name="dictionary" required="true" type="uaic.miss.model.Dictionary" description="The database." %>

<fmt:setBundle basename="messages"/>
<fmt:setLocale value="${request.getLocale()}" scope="page"/>

<table>
    <thead>
    <th scope="col">
        <fmt:message key="RESULT_TABLE_WORD_COLUMN_NAME"/>
    </th>
    <th scope="col">
        <fmt:message key="RESULT_TABLE_DEFINITION_COLUMN_NAME"/>
    </th>
    <th scope="col">
        <fmt:message key="RESULT_TABLE_LANGUAGE_COLUMN_NAME"/>
    </th>
    <th scope="col">
        <fmt:message key="RESULT_TABLE_LANGUAGE_DATE_NAME"/>
    </th>
    </thead>
    <tbody>
    <c:forEach var="dictionaryEntry" items="${dictionary.getEntries()}">
        <c:if test="${language != null ? dictionaryEntry.getLanguage().equals(language) : true }">
            <tr>
                <td>
                    <c:out value="${dictionaryEntry.getWord()}"></c:out>
                </td>
                <td>
                    <c:out value="${dictionaryEntry.getDefinition()}"></c:out>
                </td>
                <td>
                    <c:out value="${dictionaryEntry.getLanguage()}"></c:out>
                </td>
                <td>
                    <fmt:formatDate value="${dictionaryEntry.getInsertionDate()}" dateStyle="full"/>
                </td>
            </tr>
        </c:if>
    </c:forEach>
    </tbody>
</table>

