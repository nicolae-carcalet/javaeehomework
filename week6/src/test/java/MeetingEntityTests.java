import domain.MeetingEntity;
import domain.MeetingLocation;
import domain.PersonEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import repositories.MeetingRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class MeetingEntityTests {
    private final static int TEST_MEETING_ENTITY_ID = 12;
    private final static String TEST_MEETING_ENTITY_TOPIC = "test";
    private final static LocalDateTime TEST_MEETING_ENTITY_START_TIME = LocalDateTime.of(2020, 11, 20, 12, 12, 12);
    private final static LocalDateTime TEST_MEETING_ENTITY_END_TIME = LocalDateTime.of(2020, 11, 20, 15, 0, 0);
    private final static MeetingLocation TEST_MEETING_ENTITY_LOCATION = new MeetingLocation(13, "Zurich", "Conference");
    private final static Set<PersonEntity> TEST_MEETING_ENTITY_PERSONS = Set.of(new PersonEntity(14, "John Wick", null));

    private static MeetingEntity testEntity = new MeetingEntity(TEST_MEETING_ENTITY_ID,
            TEST_MEETING_ENTITY_TOPIC, TEST_MEETING_ENTITY_START_TIME,
            TEST_MEETING_ENTITY_END_TIME, TEST_MEETING_ENTITY_LOCATION, TEST_MEETING_ENTITY_PERSONS);
    private static List<MeetingEntity> allMeetings = List.of(testEntity);

    private static MeetingRepository mockedMeetingRepository = mock(MeetingRepository.class);

    @BeforeAll
    public static void initMock() {
        doNothing().when(mockedMeetingRepository).createMeeting(anyObject());
        doReturn(allMeetings).when(mockedMeetingRepository).getAllMeetings();
        doReturn(testEntity).when(mockedMeetingRepository).findMeetingById(anyInt());
        doAnswer(invocationOnMock -> {
            MeetingEntity meetingEntity = invocationOnMock.getArgumentAt(0, MeetingEntity.class);
            allMeetings = List.of(meetingEntity);
            return null;
        }).when(mockedMeetingRepository).updateMeeting(anyObject());
    }

    @Test
    public void meetingEntityIsInsertedInDB() {
        mockedMeetingRepository.createMeeting(testEntity);
        List<MeetingEntity> allMeetings = mockedMeetingRepository.getAllMeetings();
        Assertions.assertTrue(allMeetings.stream().anyMatch(meetingEntity -> meetingEntity.getTopic().equals(testEntity.getTopic())));
    }

    @Test
    public void meetingCanBeReadFromDB() {
        MeetingEntity meetingEntity = mockedMeetingRepository.findMeetingById(testEntity.getId());
        Assertions.assertNotNull(meetingEntity);
        Assertions.assertEquals(meetingEntity.getId(), testEntity.getId());
    }

    @Test
    public void meetingIsUpdatedInDB() {
        MeetingEntity meetingEntity = mockedMeetingRepository.findMeetingById(testEntity.getId());
        meetingEntity.setTopic("another");
        mockedMeetingRepository.updateMeeting(meetingEntity);
        Assertions.assertEquals("another", mockedMeetingRepository.findMeetingById(testEntity.getId()).getTopic());
    }
}
