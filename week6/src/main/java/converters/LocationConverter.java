package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import domain.MeetingLocation;

@FacesConverter("meetingLocationConverter")
public class LocationConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return "";
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        MeetingLocation meetingLocation = (MeetingLocation) value;
        return meetingLocation.getName();
    }
}
