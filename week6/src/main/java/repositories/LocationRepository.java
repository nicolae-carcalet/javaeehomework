package repositories;

import domain.MeetingLocation;
import domain.PersonEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class LocationRepository {
    private static final String GET_ALL_LOCATIONS_QUERY = "select m from MeetingLocation as m";

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MeetingPU", null);
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    public List<MeetingLocation> getAllLocations() {
        return entityManager.createQuery(GET_ALL_LOCATIONS_QUERY, MeetingLocation.class).getResultList();
    }

}
