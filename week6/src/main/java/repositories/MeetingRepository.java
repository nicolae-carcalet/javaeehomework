package repositories;

import domain.MeetingEntity;
import domain.SearchRestrictions;

import javax.faces.bean.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class MeetingRepository {
    private static final String GET_ALL_MEETINGS_QUERY = "select m from MeetingEntity as m";

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MeetingPU", null);
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    public List<MeetingEntity> getAllMeetings() {
        return entityManager.createQuery(GET_ALL_MEETINGS_QUERY, MeetingEntity.class).getResultList();
    }

    public void createMeeting(MeetingEntity meetingEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(meetingEntity);
        entityManager.getTransaction().commit();
    }

    public MeetingEntity findMeetingById(int meetingId) {
        return entityManager.find(MeetingEntity.class, meetingId);
    }

    public void updateMeeting(MeetingEntity meetingEntity) {
        // Needed only for the CRUD tests because we're mocking the repository there and there's no way
        // in which the entitymanager could automatically update the entity itself.
    }

    public List<MeetingEntity> search(SearchRestrictions searchRestrictions) {
        CriteriaQuery<MeetingEntity> searchQuery = buildSearchCriteriaApiQuery(searchRestrictions);
        return entityManager.createQuery(searchQuery).getResultList();
    }

    private CriteriaQuery<MeetingEntity> buildSearchCriteriaApiQuery(SearchRestrictions searchRestrictions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MeetingEntity> query = criteriaBuilder.createQuery(MeetingEntity.class);
        Root<MeetingEntity> root = query.from(MeetingEntity.class);
        query = query.select(root);
        List<Predicate> restrictions = buildSearchRestrictions(criteriaBuilder, root, searchRestrictions);
        query = query.where(restrictions.toArray(new Predicate[0]));
        return query;
    }

    private List<Predicate> buildSearchRestrictions(CriteriaBuilder criteriaBuilder, Root<MeetingEntity> root, SearchRestrictions searchRestrictions) {
        List<Predicate> restrictions = new ArrayList<>();
        if (searchRestrictions.isSearchForTopic()) {
            restrictions.add(criteriaBuilder.like(root.get("topic"), searchRestrictions.getSearchTopic()));
        }
        if (searchRestrictions.isSearchForStartDate()) {
            restrictions.add(criteriaBuilder.equal(root.get("startTime"), searchRestrictions.getSearchStartDate()));
        }
        if (searchRestrictions.isSearchForEndDate()) {
            restrictions.add(criteriaBuilder.equal(root.get("endTime"), searchRestrictions.getSearchEndDate()));
        }
        return restrictions;
    }
}
