package repositories;

import domain.MeetingEntity;
import domain.PersonEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PersonRepository {
    private static final String GET_ALL_PERSONS_QUERY = "select m from PersonEntity as m";

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MeetingPU", null);
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    public List<PersonEntity> getAllPersons() {
        return entityManager.createQuery(GET_ALL_PERSONS_QUERY, PersonEntity.class).getResultList();
    }
}
