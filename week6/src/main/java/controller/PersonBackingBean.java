package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "person")
@SessionScoped
public class PersonBackingBean {
    private String personName;
    private boolean hasEnteredUserName;

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public boolean getHasEnteredUserName() {
        return hasEnteredUserName;
    }

    public void setHasEnteredUserName(boolean hasEnteredUserName) {
        this.hasEnteredUserName = hasEnteredUserName;
    }

    public void searchForMeetings() {
        hasEnteredUserName = true;
    }
}
