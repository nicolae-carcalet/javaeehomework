package controller;

import domain.MeetingEntity;
import domain.MeetingLocation;
import domain.PersonEntity;
import domain.SearchRestrictions;
import repositories.LocationRepository;
import repositories.MeetingRepository;
import repositories.PersonRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ManagedBean(name = "meetings")
@SessionScoped
public class MeetingBackingBean {
    private MeetingEntity newMeeting = new MeetingEntity(0, "", null, null, null, null);
    private List<String> selectedPersonsIds = new ArrayList<>();
    private int selectedMeetingId;
    private List<MeetingEntity> allMeetings;
    private List<PersonEntity> personEntities;
    private List<MeetingLocation> meetingLocations;
    private List<MeetingEntity> foundMeetings;
    private MeetingEntity editMeeting = new MeetingEntity(0, "", null, null, null, null);

    private MeetingRepository meetingRepository = new MeetingRepository();
    private PersonRepository personRepository = new PersonRepository();
    private LocationRepository locationRepository = new LocationRepository();

    private boolean searchForTopic;
    private boolean searchForStartDate;
    private boolean searchForEndDate;
    private boolean searchForLocation;
    private boolean searchForPerson;

    public List<MeetingEntity> getAllMeetings() {
        allMeetings = meetingRepository.getAllMeetings();
        return allMeetings;
    }

    public List<MeetingLocation> getAllLocations() {
        meetingLocations = locationRepository.getAllLocations();
        return meetingLocations;
    }

    public void createMeeting() {
        MeetingEntity meetingEntity = buildMeetingEntity();
        meetingRepository.createMeeting(meetingEntity);
    }

    private MeetingEntity buildMeetingEntity() {
        String meetingTopic = newMeeting.getTopic();
        LocalDateTime meetingStartDate = newMeeting.getStartTime();
        LocalDateTime meetingDuration = newMeeting.getEndTime();
        MeetingLocation meetingLocation = meetingLocations.stream()
                .filter(meetingLocationEntity -> meetingLocationEntity.getId() == selectedMeetingId)
                .findFirst().get();
        Set<PersonEntity> attendingPersons = personEntities.stream()
                .filter(personEntity -> selectedPersonsIds.contains(String.valueOf(personEntity.getPersonId())))
                .collect(Collectors.toSet());
        return new MeetingEntity(0, meetingTopic, meetingStartDate, meetingDuration, meetingLocation, attendingPersons);
    }

    public void updateMeeting() {
    }

    public List<PersonEntity> getAllPersons() {
        personEntities = personRepository.getAllPersons();
        return personEntities;
    }

    public List<String> getSelectedPersonsIds() {
        return selectedPersonsIds;
    }

    public void setSelectedPersonsIds(List<String> selectedPersonsIds) {
        this.selectedPersonsIds = selectedPersonsIds;
    }

    public int getSelectedMeetingId() {
        return selectedMeetingId;
    }

    public void setSelectedMeetingId(int selectedMeetingId) {
        this.selectedMeetingId = selectedMeetingId;
    }

    public MeetingEntity getNewMeeting() {
        return newMeeting;
    }

    public void setNewMeeting(MeetingEntity newMeeting) {
        this.newMeeting = newMeeting;
    }

    public MeetingEntity getEditMeeting() {
        return editMeeting;
    }

    public boolean isSearchForTopic() {
        return searchForTopic;
    }

    public void setSearchForTopic(boolean searchForTopic) {
        this.searchForTopic = searchForTopic;
    }

    public boolean isSearchForStartDate() {
        return searchForStartDate;
    }

    public void setSearchForStartDate(boolean searchForStartDate) {
        this.searchForStartDate = searchForStartDate;
    }

    public boolean isSearchForEndDate() {
        return searchForEndDate;
    }

    public void setSearchForEndDate(boolean searchForEndDate) {
        this.searchForEndDate = searchForEndDate;
    }

    public boolean isSearchForLocation() {
        return searchForLocation;
    }

    public void setSearchForLocation(boolean searchForLocation) {
        this.searchForLocation = searchForLocation;
    }

    public boolean isSearchForPerson() {
        return searchForPerson;
    }

    public void setSearchForPerson(boolean searchForPerson) {
        this.searchForPerson = searchForPerson;
    }

    public List<MeetingEntity> getFoundMeetings() {
        return foundMeetings;
    }

    public void setEditMeeting(MeetingEntity editMeeting) {
        try {
            this.editMeeting = editMeeting;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void searchForMeetings() {
        SearchRestrictions searchRestrictions = buildSearchRestrictions();
        foundMeetings = meetingRepository.search(searchRestrictions);
    }

    private SearchRestrictions buildSearchRestrictions() {
        SearchRestrictions searchRestrictions = new SearchRestrictions();
        searchRestrictions.setSearchForTopic(searchForTopic);
        searchRestrictions.setSearchTopic(editMeeting.getTopic());
        searchRestrictions.setSearchForStartDate(searchForStartDate);
        searchRestrictions.setSearchStartDate(editMeeting.getStartTime());
        searchRestrictions.setSearchForEndDate(searchForEndDate);
        searchRestrictions.setSearchEndDate(editMeeting.getEndTime());
        return searchRestrictions;
    }

    public void getMeeting() {
        // TERRIBLE!!!
        this.getAllMeetings();
        for (MeetingEntity meetingEntity : allMeetings) {
            if (meetingEntity.getId() == selectedMeetingId) {
                editMeeting = meetingEntity;
                break;
            }
        }
    }
}
