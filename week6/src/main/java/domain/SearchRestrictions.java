package domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SearchRestrictions {
    private boolean searchForTopic;
    private boolean searchForStartDate;
    private boolean searchForEndDate;
    private String searchTopic;
    private LocalDateTime searchStartDate;
    private LocalDateTime searchEndDate;
}
