package model;

public class MeetingLocation {
    private int id;
    private String name;
    private String locationClass;

    public MeetingLocation(int id, String name, String locationClass) {
        this.id = id;
        this.name = name;
        this.locationClass = locationClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationClass() {
        return locationClass;
    }

    public void setLocationClass(String locationClass) {
        this.locationClass = locationClass;
    }

    @Override
    public String toString() {
        return "MeetingLocation [id=" + id + ", name=" + name + ", locationClass=" + locationClass + "]";
    }
}