package model;

import java.time.LocalDateTime;

public class MeetingEntity {
    private int meetingId;
    private String topic;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private MeetingLocation meetingLocation;

    public MeetingEntity(int meetingId, String topic, LocalDateTime startTime, LocalDateTime endTime,
                         MeetingLocation meetingLocation) {
        super();
        this.meetingId = meetingId;
        this.topic = topic;
        this.startTime = startTime;
        this.endTime = endTime;
        this.meetingLocation = meetingLocation;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public MeetingLocation getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(MeetingLocation meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    @Override
    public String toString() {
        return "MeetingEntity [meetingId=" + meetingId + ", topic=" + topic + ", startTime=" + startTime + ", endTime="
                + endTime + ", meetingLocation=" + meetingLocation + "]";
    }

}
