package controller;

import org.postgresql.ds.PGPoolingDataSource;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

@ApplicationScoped
@ManagedBean(name = "databaseService", eager = true)
public class DatabaseService {
    private static final String CONNECTION_POOL_JNDI_NAME = "jdbc/DbPool";
    private DataSource poolingDataSource;

    public DatabaseService() throws SQLException, ClassNotFoundException, NamingException {
        initPoolingDatasource();
    }

    private void initPoolingDatasource() throws NamingException {
        InitialContext ctx = new InitialContext();
        Context initCtx = (Context) ctx.lookup("java:/comp/env");
        poolingDataSource = (DataSource) initCtx.lookup(CONNECTION_POOL_JNDI_NAME);
    }

    public Connection getDatabaseConnection() throws SQLException {
        return poolingDataSource.getConnection();
    }
}
