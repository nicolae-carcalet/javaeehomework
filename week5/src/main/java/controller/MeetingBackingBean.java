package controller;

import model.MeetingEntity;
import model.MeetingLocation;
import model.PersonEntity;
import utils.MeetingUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "meetings")
@SessionScoped
public class MeetingBackingBean {
    private static final String MEETINGS_RETRIEVAL_QUERY = "select * from meeting inner join locations on meeting.meeting_location = locations.id";
    private static final String MEETING_LOCATION_QUERY = "select * from locations where id = %d";
    private static final String ALL_MEETING_LOCATION_QUERY = "select * from locations";
    private static final String PERSONS_SELECT_ALL_QUERY = "select * from persons";
    private static final String INSERT_MEETING_QUERY = "insert into meeting(topic, start_date, end_date, meeting_location) values ('%s', '%s', '%s', %d)";
    private static final String UPDATE_MEETING_QUERY = "update meeting set topic='%s', start_date='%s', end_date='%s', meeting_location='%d' where id = %d";
    private static final String INSERTION_ID_QUERY = "select max(id) from meeting";
    private static final String LINKING_TABLE_INSERT_QUERY = "insert into linking(meeting_id, person_id) values (%d, %d)";
    private static final String LINKING_TABLE_DELETE_QUERY = "delete from linking where meeting_id = %d";

    private MeetingEntity newMeeting = new MeetingEntity(0, "", null, null, null);
    private List<String> selectedPersonsIds = new ArrayList<>();
    private int selectedMeetingId;
    private List<MeetingEntity> allMeetings;
    private List<MeetingEntity> allLocations;
    private MeetingEntity editMeeting;

    @ManagedProperty(value = "#{databaseService}")
    private DatabaseService databaseService;

    public List<MeetingEntity> getAllMeetings() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = databaseService.getDatabaseConnection();
            statement = connection.createStatement();

            List<MeetingEntity> meetingEntities = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(MEETINGS_RETRIEVAL_QUERY);
            while (!resultSet.isClosed() && resultSet.next()) {
                MeetingEntity meetingEntity = MeetingUtils.getMeetingEntityFromResultSetCursor(resultSet);
                meetingEntities.add(meetingEntity);
            }
            allMeetings = meetingEntities;
            resultSet.close();
            return meetingEntities;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
        return null;
    }

    public List<MeetingLocation> getAllLocations() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = databaseService.getDatabaseConnection();
            statement = connection.createStatement();
            List<MeetingLocation> allLocations = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(ALL_MEETING_LOCATION_QUERY);
            while (resultSet.next()) {
                int locationId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String locationClass = resultSet.getString(3);
                allLocations.add(new MeetingLocation(locationId, name, locationClass));
            }
            return allLocations;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
        return null;
    }

    public void createMeeting() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = databaseService.getDatabaseConnection();
            statement = connection.createStatement();
            String sqlQuery = getInsertMeetingQuery();
            statement.execute(sqlQuery);
            int meetingId = getNewMeetingInsertionId(statement);
            updateLinkingTable(statement, meetingId);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
    }

    private String getInsertMeetingQuery() {
        String meetingTopic = newMeeting.getTopic();
        LocalDateTime meetingStartDate = newMeeting.getStartTime();
        LocalDateTime meetingDuration = newMeeting.getEndTime();
        return String.format(INSERT_MEETING_QUERY, meetingTopic, meetingStartDate, meetingDuration, selectedMeetingId);
    }

    public void updateMeeting() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = databaseService.getDatabaseConnection();
            statement = connection.createStatement();
            String sqlQuery = getUpdateMeetingQuery();
            statement.execute(sqlQuery);
            int meetingId = getNewMeetingInsertionId(statement);
            deleteMeetingFromLinkingTable(statement, meetingId);
            updateLinkingTable(statement, meetingId);
            statement.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
    }

    private void deleteMeetingFromLinkingTable(Statement statement, int meetingId) throws SQLException {
        String query = String.format(LINKING_TABLE_DELETE_QUERY, meetingId);
        statement.execute(query);
    }

    private String getUpdateMeetingQuery() {
        int meetingId = editMeeting.getMeetingId();
        String meetingTopic = editMeeting.getTopic();
        LocalDateTime meetingStartDate = editMeeting.getStartTime();
        LocalDateTime meetingDuration = editMeeting.getEndTime();
        int meetingLocationId = selectedMeetingId;
        return String.format(UPDATE_MEETING_QUERY, meetingTopic, meetingStartDate, meetingDuration, meetingLocationId, meetingId);
    }

    private int getNewMeetingInsertionId(Statement statement) throws SQLException {
        // HACK, FIX IT!
        ResultSet cursor = statement.executeQuery(INSERTION_ID_QUERY);
        if (cursor.next()) {
            return cursor.getInt(1);
        }
        return -1;
    }

    private void updateLinkingTable(Statement statement, int meetingId) throws SQLException {
        for (String selectedPersonId : selectedPersonsIds) {
            String linkingTableInsertQuery = String.format(LINKING_TABLE_INSERT_QUERY, meetingId,
                    Integer.parseInt(selectedPersonId));
            statement.execute(linkingTableInsertQuery);
        }
    }

    public List<PersonEntity> getAllPersons() throws SQLException {
        List<PersonEntity> personEntities = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = databaseService.getDatabaseConnection();
            statement = connection.createStatement();
            ResultSet cursor = statement.executeQuery(PERSONS_SELECT_ALL_QUERY);
            while (cursor.next()) {
                personEntities.add(getPersonEntityFromCurrentRow(cursor));
            }
            statement.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
        return personEntities;
    }

    private PersonEntity getPersonEntityFromCurrentRow(ResultSet cursor) throws SQLException {
        int personId = cursor.getInt(1);
        String personName = cursor.getString(2);
        return new PersonEntity(personId, personName);
    }

    public List<String> getSelectedPersonsIds() {
        return selectedPersonsIds;
    }

    public void setSelectedPersonsIds(List<String> selectedPersonsIds) {
        this.selectedPersonsIds = selectedPersonsIds;
    }

    public int getSelectedMeetingId() {
        return selectedMeetingId;
    }

    public void setSelectedMeetingId(int selectedMeetingId) {
        this.selectedMeetingId = selectedMeetingId;
    }

    public void setDatabaseService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public MeetingEntity getNewMeeting() {
        return newMeeting;
    }

    public void setNewMeeting(MeetingEntity newMeeting) {
        this.newMeeting = newMeeting;
    }

    public MeetingEntity getEditMeeting() {
        return editMeeting;
    }

    public void setEditMeeting(MeetingEntity editMeeting) {
        try {
            this.editMeeting = editMeeting;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void getMeeting() {
        if (allMeetings.isEmpty()) {
            return;
        }

        for (MeetingEntity meetingEntity : allMeetings) {
            if (meetingEntity.getMeetingId() == selectedMeetingId) {
                editMeeting = meetingEntity;
                break;
            }
        }
    }
}
