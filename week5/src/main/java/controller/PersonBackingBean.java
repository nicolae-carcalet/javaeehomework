package controller;

import model.MeetingEntity;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
import utils.MeetingUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "person")
@SessionScoped
public class PersonBackingBean {
    private static final String SELECT_USER_BY_USERNAME = "select * from persons where name = '%s'";
    private static final String SELECT_MEETINGS_BY_USER_ID = "select * from meeting inner join linking on meeting.id = linking.meeting_id where linking.person_id = %d";
    private String personName;
    private TimelineModel<String, ?> meetings;
    private boolean hasEnteredUserName;

    @ManagedProperty(value = "#{databaseService}")
    private DatabaseService databaseService;

    public TimelineModel<String, ?> getMeetings() {
        buildTimelineModel();
        return meetings;
    }

    private void buildTimelineModel() {
        try (Statement statement = databaseService.getDatabaseConnection().createStatement()) {
            int userId = getPersonIdByUserName(statement, personName);
            List<MeetingEntity> meetingsForUser = getMeetingsForPerson(statement, userId);
            meetings = new TimelineModel<>();
            for (MeetingEntity meetingEntity : meetingsForUser) {
                meetings.add(TimelineEvent.<String>builder().data(meetingEntity.getTopic()).startDate(meetingEntity.getStartTime()).build());
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private int getPersonIdByUserName(Statement statement, String username) throws SQLException {
        String sqlQuery = String.format(SELECT_USER_BY_USERNAME, username);
        ResultSet cursor = statement.executeQuery(sqlQuery);

        if (cursor.next()) {
            return cursor.getInt(1);
        }
        return -1;
    }

    private List<MeetingEntity> getMeetingsForPerson(Statement statement, int userId) throws SQLException {
        String sqlQuery = String.format(SELECT_MEETINGS_BY_USER_ID, userId);
        ResultSet cursor = statement.executeQuery(sqlQuery);

        List<MeetingEntity> meetingEntities = new ArrayList<>();
        while (cursor.next()) {
            meetingEntities.add(MeetingUtils.getMeetingEntityFromResultSetCursor(cursor));
        }

        return meetingEntities;
    }

    public void setDatabaseService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public boolean getHasEnteredUserName() {
        return hasEnteredUserName;
    }

    public void setHasEnteredUserName(boolean hasEnteredUserName) {
        this.hasEnteredUserName = hasEnteredUserName;
    }

    public void searchForMeetings() {
        hasEnteredUserName = true;
        System.out.println(personName);
    }
}
