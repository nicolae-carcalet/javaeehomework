package utils;

import model.MeetingEntity;
import model.MeetingLocation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class MeetingUtils {
    public static MeetingEntity getMeetingEntityFromResultSetCursor(ResultSet cursor) throws SQLException {
        int meetingId = cursor.getInt(1);
        String topic = cursor.getString(2);
        LocalDateTime start_time = cursor.getTimestamp(3).toLocalDateTime();
        LocalDateTime duration = cursor.getTimestamp(4).toLocalDateTime();
        int locationId = cursor.getInt(6);
        String locationName = cursor.getString(7);
        String locationClass = cursor.getString(8);
        MeetingLocation meetingLocation = new MeetingLocation(locationId, locationName, locationClass);
        return new MeetingEntity(meetingId, topic, start_time, duration, meetingLocation);
    }
}
