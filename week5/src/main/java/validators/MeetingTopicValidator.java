package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@FacesValidator("meetingTopicValidator")
public class MeetingTopicValidator implements Validator {

    private static final String[] forbiddenWords = new String[]{"piss", "kill", "steal"};

    @Override
    public void validate(FacesContext facesContext, UIComponent component, Object topicInputField)
            throws ValidatorException {
        UIInput input1 = (UIInput) component;
        String topic = input1.getSubmittedValue().toString();
        String[] topicWords = topic.split(" ");
        for (String topicWord : topicWords) {
            for (String forbiddenWord : forbiddenWords) {
                if (forbiddenWord.equalsIgnoreCase(topicWord)) {
                    FacesMessage msg = new FacesMessage("Forbidden word");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }
            }
        }
    }
}
