package solver;

import model.MeetingEntity;
import model.PersonEntity;
import org.chocosolver.solver.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class MeetingProblemSchedulingModelBuilder {
    private Model model;

    public Model buildModel(List<MeetingEntity> meetingEntities, List<PersonEntity> personEntities) {
        model = new Model();
        addVariables(meetingEntities);
        addConstraints(meetingEntities);
        return model;
    }

    private void addVariables(List<MeetingEntity> meetingEntities) {
        for (MeetingEntity meetingEntity : meetingEntities) {
            String unknownVariableName = "Meeting" + meetingEntity.getMeetingId();

            // TODO: This is VERY, VERY BAD. Change it
            int startTimeInMinutes = (int) meetingEntity.getStartTime().toEpochSecond(ZoneOffset.UTC) / 60;
            int endTimeInMinutes = (int) meetingEntity.getEndTime().toEpochSecond(ZoneOffset.UTC) / 60;
            model.intVar(unknownVariableName, startTimeInMinutes, endTimeInMinutes);
        }
    }

    private void addConstraints(List<MeetingEntity> meetingEntities) {

    }

    public static void main(String[] args) {
        MeetingProblemSchedulingModelBuilder meetingProblemSchedulingModelBuilder = new MeetingProblemSchedulingModelBuilder();
        List<MeetingEntity> entities = new ArrayList<>();
    }
}
