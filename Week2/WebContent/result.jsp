<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="uaic.miss.*" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Results</title>
</head>
<body>
<p>Hooray! Your entry was added successfully into our knowledge base</p>
<p>Our current information:</p>

<table>
    <thead>
    <th scope="col">Word</th>
    <th scope="col">Definition</th>
    <th scope="col">Language</th>
    </thead>
    <tbody>
    <%
        Dictionary dictionary = (Dictionary) request.getAttribute("dictionary");

        for (DictionaryEntry dictEntry : dictionary) {
            out.write("<tr>");
            out.write("<td>" + dictEntry.getWord() + "</td>");
            out.write("<td>" + dictEntry.getDefinition() + "</td>");
            out.write("<td>" + dictEntry.getLanguage() + "</td>");
            out.write("</tr>");
        }
    %>
    </tbody>
</table>
</body>
</html>
