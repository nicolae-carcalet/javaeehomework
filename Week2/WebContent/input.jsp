<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Input page</title>
</head>
<body>
<form action="add" method="POST">
    Word:
    <input type="text" name="word">
    <br>

    Definition:
    <input type="text" name="definition">

    <br>

    Language:
    <select name="language">
        <%
            List<String> languages = (List<String>) request.getAttribute("languages");
            String hasSelectedLanguageCookie = (String) request.getAttribute("hasSelectedLangCookie");
            String selectedLanguage = (String) request.getAttribute("selectedLanguageCookie");
            if (languages != null) {
                if (hasSelectedLanguageCookie == null) {
                    for (String lang : languages) {
                        out.write("<option value=" + lang + ">" + lang + "</option>");
                    }
                } else {
                    for (String lang : languages) {
                        if (lang.equals(selectedLanguage)) {
                            out.write("<option selected=\"selected\" value=" + lang + ">" + lang + "</option>");
                        } else {
                            out.write("<option value=" + lang + ">" + lang + "</option>");
                        }
                    }
                }
            }
        %>
    </select>

    <br>

    Captcha:
    <img src="captcha">
    <br>
    <input type="text" name="captcha">
    <br>
    <input type="submit" value="Add">
</form>
</body>
</html>
