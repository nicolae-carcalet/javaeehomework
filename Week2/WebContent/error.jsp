<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error page</title>
</head>
<body>
<p>Sorry, we couldn't process you're request.</p>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
    out.write("<p>" + errorMessage + "</p>");
%>
</body>
</html>
