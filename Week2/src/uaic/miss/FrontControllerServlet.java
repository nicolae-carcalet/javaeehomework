package uaic.miss;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "FrontController", urlPatterns = {"/index.html", "/add"})
public class FrontControllerServlet extends HttpServlet {
    private static final String INPUT_JSP_FILENAME = "/input.jsp";
    private static final String ERROR_PAGE = "/error.jsp";
    private static final String RESULT_PAGE = "/result.jsp";
    private static final String SUPPORTED_LANGUAGES_ATTRIBUTE = "languages";
    private static final String ERROR_MESSAGE_ATTRIBUTE = "errorMessage";
    private static final String WORD_REQUEST_PARAMETER = "word";
    private static final String DEFINITION_REQUEST_PARAMETER = "definition";
    private static final String LANGUAGE_REQUEST_PARAMETER = "language";
    private static final String DICTIONARY_REQUEST_PARAMETER = "dictionary";
    private static final String SELECTED_LANGUAGE_COOKIE_NAME = "selectedLanguage";
    private static final String DUPLICATE_ENTRY_ERROR_MESSAGE = "The entry you've entered is already present";
    private static final String INVALID_CAPTCHA_ERROR_MESSAGE = "The answer to the captcha is not correct";
    private static final String HAS_SELECTED_LANGUAGE_COOKIE_ATTRIBUTE = "hasSelectedLangCookie";
    private static final String SELECTED_LANGUAGE_COOKIE_ATTRIBUTE = "selectedLanguage";
    private static final String CAPTCHA_COOKIE_NAME = "captchaResult";
    private static final String CAPTCHA_PARAMETER = "captcha";
    private Dictionary dictionary;
    private SupportedLanguages supportedLanguages;

    @Override
    public void init() throws ServletException {
        dictionary = new Dictionary();
        supportedLanguages = new SupportedLanguages();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (hasLanguageCookie(req)) {
            String selectedLanguage = getSelectedLanguageFromCookie(req);
            req.setAttribute(HAS_SELECTED_LANGUAGE_COOKIE_ATTRIBUTE, "true");
            req.setAttribute(SELECTED_LANGUAGE_COOKIE_ATTRIBUTE, selectedLanguage);
        }
        req.setAttribute(SUPPORTED_LANGUAGES_ATTRIBUTE, supportedLanguages.getAllSupportedLanguages());
        req.getRequestDispatcher(INPUT_JSP_FILENAME).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String word = req.getParameter(WORD_REQUEST_PARAMETER);
        String definition = req.getParameter(DEFINITION_REQUEST_PARAMETER);
        String language = req.getParameter(LANGUAGE_REQUEST_PARAMETER);

        if (!isCaptchaCorrect(req)) {
            redirectToErrorPageWithMessage(req, resp, INVALID_CAPTCHA_ERROR_MESSAGE);
            return;
        }

        if (dictionary.isEntryAlreadyPresent(word, definition, language)) {
            redirectToErrorPageWithMessage(req, resp, DUPLICATE_ENTRY_ERROR_MESSAGE);
        } else {
            addWordToDictionary(word, definition, language);
            req.setAttribute(DICTIONARY_REQUEST_PARAMETER, dictionary);
            if (!hasLanguageCookie(req)) {
                addLanguageCookie(resp, language);
            }

            req.getRequestDispatcher(RESULT_PAGE).forward(req, resp);
        }
    }

    private String getSelectedLanguageFromCookie(HttpServletRequest req) {
        Cookie languageCookie = getCookieWithName(req, SELECTED_LANGUAGE_COOKIE_ATTRIBUTE);
        return languageCookie.getValue();
    }

    private void addLanguageCookie(HttpServletResponse resp, String language) {
        Cookie selectedLanguageCookie = new Cookie(SELECTED_LANGUAGE_COOKIE_NAME, language);
        resp.addCookie(selectedLanguageCookie);
    }

    private boolean isCaptchaCorrect(HttpServletRequest req) {
        String userCaptchaValue = req.getParameter(CAPTCHA_PARAMETER);
        Cookie captchaCookie = getCookieWithName(req, CAPTCHA_COOKIE_NAME);

        if (captchaCookie == null) {
            return false;
        }
        System.out.println(userCaptchaValue);
        System.out.println(captchaCookie.getValue());

        return userCaptchaValue.equals(captchaCookie.getValue());
    }

    private void addWordToDictionary(String word, String definition, String language) {
        DictionaryEntry dictionaryEntry = new DictionaryEntry(word, definition, language);
        dictionary.addDictionaryEntry(dictionaryEntry);
    }

    private void redirectToErrorPageWithMessage(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        req.setAttribute(ERROR_MESSAGE_ATTRIBUTE, errorMessage);
        req.getRequestDispatcher(ERROR_PAGE).forward(req, resp);
    }

    private boolean hasLanguageCookie(HttpServletRequest req) {
        return getCookieWithName(req, SELECTED_LANGUAGE_COOKIE_NAME) != null;
    }

    private Cookie getCookieWithName(HttpServletRequest req, String cookieName) {
        Cookie[] cookies = req.getCookies();

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)) {
                return cookie;
            }
        }
        return null;
    }
}
