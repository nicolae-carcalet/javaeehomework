package uaic.miss;

import java.util.*;

public class Dictionary implements Iterable<DictionaryEntry> {
    private Set<DictionaryEntry> dictionaryEntries;

    public Dictionary() {
        dictionaryEntries = new HashSet<>();
    }

    public synchronized void addDictionaryEntry(DictionaryEntry dictionaryEntry) {
        if (dictionaryEntry != null) {
            dictionaryEntries.add(dictionaryEntry);
        }
    }

    public boolean isEntryAlreadyPresent(String word, String definition, String language) {
        return dictionaryEntries.contains(new DictionaryEntry(word, definition, language));
    }

    @Override
    public Iterator<DictionaryEntry> iterator() {
        return dictionaryEntries.iterator();
    }
}
